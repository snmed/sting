package sting

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"runtime/debug"
	"strconv"
	"testing"
)

func Test_container_HandlerFunc(t *testing.T) {

	type (
		A struct {
			str string
		}
		B struct {
			id int
		}
		C struct {
			a A
			b *B
		}
		I interface {
			Say()
		}
	)

	type reg struct {
		fn  interface{}
		scp Scope
	}
	type args struct {
		handler InjectionHandler
	}
	gotDeps := &[]interface{}{}
	tests := []struct {
		name      string
		regs      []reg
		args      args
		wantDeps  []interface{}
		wantPanic bool
	}{
		{
			name: "Resolve all dependencies",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, r *http.Request, c C, a *A) { *gotDeps = append(*gotDeps, c, a) },
			},
			wantDeps: []interface{}{
				C{a: A{"A"}, b: &B{1}},
				&A{"A"},
			},
		},
		{
			name: "Panics because no injection handler",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, c C, a *A) { *gotDeps = append(*gotDeps, c, a) },
			},
			wantPanic: true,
		},
		{
			name: "Panics because injection handler has unsupported types",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, r *http.Request, c C, a *A, f func()) {
					*gotDeps = append(*gotDeps, c, a)
				},
			},
			wantPanic: true,
		},
		{
			name: "Panics because of missing dependencies",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, r *http.Request, c C, a *A, t testA) { *gotDeps = append(*gotDeps, c, a) },
			},
			wantPanic: true,
		},
		{
			name: "Panics because of scope func returns nil",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func() (I, error) { return nil, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, r *http.Request, c C, a *A, i I) { *gotDeps = append(*gotDeps, c, a) },
			},
			wantPanic: true,
		},
		{
			name: "Panics because of an error while calling scopeFunc",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, fmt.Errorf("An error occurred") },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func() (I, error) { return nil, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				handler: func(w http.ResponseWriter, r *http.Request, c C, a *A, i I) { *gotDeps = append(*gotDeps, c, a) },
			},
			wantPanic: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if r := recover(); r != nil && !tt.wantPanic {
					t.Fatalf("container.HandlerFunc() panics = %v, wantPanic %v -> %v", r, tt.wantPanic, string(debug.Stack()))
				}
			}()
			gotDeps = new([]interface{})
			builder := newBuilder()
			for _, svc := range tt.regs {
				builder.Register(svc.fn, svc.scp)
			}

			container, err := builder.Build()
			if err != nil {
				t.Fatalf("No error should be returned by the builder %v", err)
			}

			handler := container.HandlerFunc(tt.args.handler)

			req, _ := http.NewRequest("GET", "/test", nil)
			rr := httptest.NewRecorder()

			handler.ServeHTTP(rr, req)

			if len(*gotDeps) != len(tt.wantDeps) {
				t.Fatalf("container.HandlerFunc() gotDeps = %v, want %v", len(*gotDeps), len(tt.wantDeps))
			}

			for i, dep := range *gotDeps {

				if !reflect.DeepEqual(dep, tt.wantDeps[i]) {
					t.Fatalf("container.HandlerFunc() = %v, want %v", dep, tt.wantDeps[i])
				}
			}
		})
	}
}

func Test_container_getNode(t *testing.T) {
	type fields struct {
		registry depMap
	}
	type args struct {
		i interface{}
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantNode *depNode
		wantOk   bool
	}{
		{
			name: "Interface pointer type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{},
				},
			},
			args: args{
				i: (*testInterfaceA)(nil),
			},
			wantNode: &depNode{},
			wantOk:   true,
		},
		{
			name: "Struct type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf(testA{}): &depNode{},
				},
			},
			args: args{
				i: testA{},
			},
			wantNode: &depNode{},
			wantOk:   true,
		},
		{
			name: "Struct ptr type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf(testA{}): &depNode{},
				},
			},
			args: args{
				i: &testA{},
			},
			wantNode: &depNode{},
			wantOk:   true,
		},
		{
			name: "Struct nil ptr type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf(testA{}): &depNode{},
				},
			},
			args: args{
				i: (*testA)(nil),
			},
			wantNode: &depNode{},
			wantOk:   true,
		},
		{
			name: "String returns depnode",
			fields: fields{
				registry: depMap{
					namedSvcKey("test"): &depNode{},
				},
			},
			args: args{
				i: "test",
			},
			wantNode: &depNode{},
			wantOk:   true,
		},
		{
			name: "Interface pointer type returns nil",
			fields: fields{
				registry: depMap{
					reflect.TypeOf((*testA)(nil)).Elem(): &depNode{},
				},
			},
			args: args{
				i: (*testInterfaceA)(nil),
			},
			wantNode: nil,
			wantOk:   false,
		},
		{
			name: "Struct type returns nil",
			fields: fields{
				registry: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)): &depNode{},
				},
			},
			args: args{
				i: testA{},
			},
			wantNode: nil,
			wantOk:   false,
		},
		{
			name: "Struct ptr type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)): &depNode{},
				},
			},
			args: args{
				i: &testA{},
			},
			wantNode: nil,
			wantOk:   false,
		},
		{
			name: "Struct nil ptr type returns depnode",
			fields: fields{
				registry: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)): &depNode{},
				},
			},
			args: args{
				i: (*testA)(nil),
			},
			wantNode: nil,
			wantOk:   false,
		},
		{
			name: "String returns depnode",
			fields: fields{
				registry: depMap{
					namedSvcKey("nothere"): &depNode{},
				},
			},
			args: args{
				i: "test",
			},
			wantNode: nil,
			wantOk:   false,
		},
		{
			name: "Int returns nil",
			fields: fields{
				registry: depMap{
					namedSvcKey("nothere"): &depNode{},
				},
			},
			args: args{
				i: 42,
			},
			wantNode: nil,
			wantOk:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &container{
				registry: tt.fields.registry,
			}
			gotNode, gotOk := c.getNode(tt.args.i, reflect.TypeOf(tt.args.i))
			if !reflect.DeepEqual(gotNode, tt.wantNode) {
				t.Errorf("container.getNode() gotNode = %v, want %v", gotNode, tt.wantNode)
			}
			if gotOk != tt.wantOk {
				t.Errorf("container.getNode() gotOk = %v, want %v", gotOk, tt.wantOk)
			}
		})
	}
}

func Test_ensureReqCtx(t *testing.T) {
	type args struct {
		ctx context.Context
		id  ContainerID
	}
	tests := []struct {
		name    string
		args    args
		wantNew bool
	}{
		{
			name: "Nil context return context with scopeCtx",
			args: args{
				ctx: nil,
				id:  ContainerID("test"),
			},
			wantNew: true,
		},
		{
			name: "Context with wrong type return context with scopeCtx",
			args: args{
				ctx: context.WithValue(context.Background(), ContainerID("test"), 42),
				id:  ContainerID("test"),
			},
			wantNew: true,
		},
		{
			name: "Context with correct type return context with scopeCtx",
			args: args{
				ctx: context.WithValue(context.Background(), ContainerID("test"), &scopeCtx{items: make(map[interface{}]interface{}, 0)}),
				id:  ContainerID("test"),
			},
			wantNew: false,
		},
		{
			name: "Context with correct type but as ptr return context with scopeCtx",
			args: args{
				ctx: context.WithValue(context.Background(), ContainerID("test"), scopeCtx{items: make(map[interface{}]interface{}, 0)}),
				id:  ContainerID("test"),
			},
			wantNew: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ensureReqCtx(tt.args.ctx, tt.args.id)
			if mm := got.Value(tt.args.id); mm == nil {
				t.Errorf("ensureReqCtx() want map for key %v, get %v", tt.args.id, mm)
			} else if g, ok := mm.(*scopeCtx); !ok {
				t.Errorf("ensureReqCtx() = %v, want *scopeCtx", g)
			} else if g.items == nil {
				t.Errorf("ensureReqCtx() = scopeCtx items is nil must be map")
			}

			if (tt.wantNew && got == tt.args.ctx) || (!tt.wantNew && got != tt.args.ctx) {
				t.Errorf("ensureReqCtx() got = %v, %v wrong context returend", got, tt.args.ctx)
			}
		})
	}
}

type testInterfaceB interface {
	Say() string
}

type testB struct {
	m string
}

func (b testB) Say() string {
	return b.m
}

func Test_container_GetService(t *testing.T) {
	type (
		A struct {
			str string
		}
		B struct {
			id int
		}
		C struct {
			a A
			b *B
		}
	)

	type reg struct {
		fn  interface{}
		scp Scope
	}
	type args struct {
		ctxs []context.Context
		svc  interface{}
		id   string
	}

	tests := []struct {
		name    string
		regs    []reg
		args    args
		wantErr bool
		wantSvc interface{}
		wantCtx context.Context
	}{
		{
			name: "GetService called but actionFunc returns error",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, fmt.Errorf("An Error") },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs: []context.Context{},
				id:   "test",
				svc:  (*testInterfaceB)(nil),
			},
			wantErr: true,
		},
		{
			name: "GetService called with not registered type returns error",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs: []context.Context{},
				id:   "test",
				svc:  (*testInterfaceA)(nil),
			},
			wantErr: true,
		},
		{
			name: "GetService called with struct returns error",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs: []context.Context{},
				id:   "test",
				svc:  testB{m: "A"},
			},
			wantErr: true,
		},
		{
			name: "GetService without context",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs: []context.Context{},
				id:   "test",
				svc:  (*testInterfaceB)(nil),
			},
			wantErr: false,
			wantSvc: testB{m: "A"},
			wantCtx: context.WithValue(context.Background(),
				ContainerID("test"),
				&scopeCtx{
					items: map[interface{}]interface{}{
						reflect.TypeOf((*testInterfaceB)(nil)).Elem(): testB{m: "A"},
					},
				}),
		},
		{
			name: "GetService with context not calling actionFunc",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs: []context.Context{
					context.WithValue(context.Background(),
						ContainerID("test"),
						&scopeCtx{
							items: map[interface{}]interface{}{
								reflect.TypeOf((*testInterfaceB)(nil)).Elem(): testB{m: "B"},
							},
						}),
				},
				id:  "test",
				svc: (*testInterfaceB)(nil),
			},
			wantErr: false,
			wantSvc: testB{m: "B"},
			wantCtx: context.WithValue(context.Background(),
				ContainerID("test"),
				&scopeCtx{
					items: map[interface{}]interface{}{
						reflect.TypeOf((*testInterfaceB)(nil)).Elem(): testB{m: "B"},
					},
				}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			builder := newBuilder()
			for _, svc := range tt.regs {
				builder.Register(svc.fn, svc.scp)
			}

			ct, err := builder.Build()
			c := ct.(*container)
			c.id = ContainerID(tt.args.id)
			if err != nil {
				t.Fatalf("No error should be returned by the builder %v", err)
			}

			svc, ctx, err := ct.GetService(tt.args.svc, tt.args.ctxs...)
			if (err != nil) != tt.wantErr {
				t.Fatalf("container.GetService() err = %v want %v", err, tt.wantErr)
			}
			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(ctx, tt.wantCtx) {
				t.Fatalf("container.GetService() ctx = %v want %v", ctx, tt.wantCtx)
			}

			if !reflect.DeepEqual(svc, tt.wantSvc) {
				t.Fatalf("container.GetService() svc = %v want %v", svc, tt.wantSvc)
			}

		})
	}
}

type testG struct {
	Num int    `di:"func=SetNum"`
	msg string `di:"func=SetMsg"`
	aa  testA
	BB  testA `di:"func=SetDoubledtestA,name=test"`
}

func (tg *testG) SetNum(a *testA) {
	tg.Num = a.a
}

func (tg *testG) SetMsg(b testB) {
	tg.msg = b.m
}

func (tg *testG) SetDoubledtestA(a testA) {
	tg.BB = testA{a: (a.a * a.a)}
}

func (tg *testG) SetAa(a testA) {
	tg.aa = a
}

func Test_container_Inject_Struct(t *testing.T) {
	type (
		A struct {
			str string
		}
		B struct {
			id int
		}
		C struct {
			a A
			b *B
		}

		TargetA struct {
			AA A
			BB *B
			CC C
			DD string
			IF testInterfaceB
		}

		TargetB struct {
			AA A
			BB *B `di:"ignore"`
			CC C
		}

		TargetC struct {
			TargetB
			BBB B
			AAA *A `di:"ignore"`
		}

		TargetD struct {
			TargetC
			IF testInterfaceB
		}

		TargetE struct {
			AA A `di:"name=test"`
		}
	)

	type reg struct {
		fn    interface{}
		scp   Scope
		named string
	}
	type args struct {
		ctxs   []context.Context
		target interface{}
		id     string
	}

	var resolved *[]interface{}

	tests := []struct {
		name       string
		regs       []reg
		regsNamed  []reg
		args       args
		wantErr    bool
		wantStruct interface{}
		isFunc     bool
		wantDeps   []interface{}
	}{
		{
			name: "Inject into struct",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs:   []context.Context{},
				id:     "test",
				target: &TargetA{},
			},
			wantErr: false,
			wantStruct: &TargetA{
				AA: A{"A"},
				BB: &B{1},
				CC: C{a: A{"A"}, b: &B{1}},
				IF: testB{m: "A"},
			},
		},
		{
			name: "Inject into struct with ignored field",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs:   []context.Context{},
				id:     "test",
				target: &TargetB{},
			},
			wantErr: false,
			wantStruct: &TargetB{
				AA: A{"A"},
				CC: C{a: A{"A"}, b: &B{1}},
			},
		},
		{
			name: "Inject into nested structs",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*B, error) { return &B{1}, nil },
					scp: ContainerScope(),
				},
				{
					fn:  func(a *A, b B) (C, error) { return C{a: *a, b: &b}, nil },
					scp: RequestScope(),
				},
				{
					fn:  func(c C) (testInterfaceB, error) { return testB{m: c.a.str}, nil },
					scp: RequestScope(),
				},
			},
			args: args{
				ctxs:   []context.Context{},
				id:     "test",
				target: &TargetD{},
			},
			wantErr: false,
			wantStruct: &TargetD{
				TargetC: TargetC{
					TargetB: TargetB{AA: A{str: "A"}, CC: C{a: A{str: "A"}, b: &B{id: 1}}},
					BBB:     B{1},
				},
				IF: testB{m: "A"},
			},
		},
		{
			name: "Inject into struct with named di",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"A"}, nil },
					scp: TransientScope(),
				},
			},
			regsNamed: []reg{
				{
					fn:    func() (A, error) { return A{"right"}, nil },
					scp:   TransientScope(),
					named: "test",
				},
			},
			args: args{
				id:     "test",
				target: &TargetE{},
			},
			wantErr: false,
			wantStruct: &TargetE{
				AA: A{str: "right"},
			},
		},
		{
			name: "Inject into struct with setters",
			regs: []reg{
				{
					fn:  func() (testA, error) { return testA{2}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*testB, error) { return &testB{"test"}, nil },
					scp: TransientScope(),
				},
			},
			regsNamed: []reg{
				{
					fn:    func() (testA, error) { return testA{3}, nil },
					scp:   TransientScope(),
					named: "test",
				},
			},
			args: args{
				id:     "test",
				target: &testG{},
			},
			wantErr: false,
			wantStruct: &testG{
				Num: 2,
				msg: "test",
				aa:  testA{2},
				BB:  testA{9},
			},
		},
		{
			name: "Inject into struct but not passed as pointer",
			regs: []reg{
				{
					fn:  func() (testA, error) { return testA{2}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*testB, error) { return &testB{"test"}, nil },
					scp: TransientScope(),
				},
			},
			args: args{
				id:     "test",
				target: testG{},
			},
			wantErr: true,
		},
		{
			name: "Inject into struct but passed an invalid pointer",
			regs: []reg{
				{
					fn:  func() (testA, error) { return testA{2}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*testB, error) { return &testB{"test"}, nil },
					scp: TransientScope(),
				},
			},
			args: args{
				id:     "test",
				target: (*testInterfaceA)(nil),
			},
			wantErr: true,
		},
		{
			name: "Inject into function ",
			regs: []reg{
				{
					fn:  func() (testA, error) { return testA{2}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (*testB, error) { return &testB{"test"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func() (A, error) { return A{"aaa"}, nil },
					scp: TransientScope(),
				},
				{
					fn:  func(b testB) (testInterfaceB, error) { return testB{m: b.Say() + " bbb"}, nil },
					scp: TransientScope(),
				},
			},
			args: args{
				id: "test",
				target: func(a *testA, b testB, i testInterfaceB, ta A) {
					*resolved = append(*resolved, a, b, i, ta)
				},
			},
			wantErr: false,
			isFunc:  true,
			wantDeps: []interface{}{
				&testA{2},
				testB{"test"},
				testB{"test bbb"},
				A{"aaa"},
			},
		},
		{
			name: "Inject into function with invalid dependencies",
			regs: []reg{
				{
					fn:  func() (A, error) { return A{"aaa"}, nil },
					scp: TransientScope(),
				},
			},
			args: args{
				id: "test",
				target: func(a *A, c chan<- int) {
					*resolved = append(*resolved, a, c)
				},
			},
			wantErr: true,
			isFunc:  true,
		},
		{
			name: "Inject into function with missing dependencies",
			args: args{
				id: "test",
				target: func(a *A) {
					*resolved = append(*resolved, a)
				},
			},
			wantErr: true,
			isFunc:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resolved = &[]interface{}{}

			builder := newBuilder()
			for _, svc := range tt.regs {
				builder.Register(svc.fn, svc.scp)
			}

			for _, svc := range tt.regsNamed {
				builder.RegisterNamed(svc.named, svc.fn, svc.scp)
			}

			ct, err := builder.Build()
			c := ct.(*container)
			c.id = ContainerID(tt.args.id)
			if err != nil {
				t.Fatalf("No error should be returned by the builder %v", err)
			}

			_, err = ct.Inject(tt.args.target, tt.args.ctxs...)
			if (err != nil) != tt.wantErr {
				t.Fatalf("container.Inject() err = %v want %v, error: %v", (err != nil), tt.wantErr, err)
			}
			if tt.wantErr {
				return
			}

			if tt.isFunc {
				if len(tt.wantDeps) != len(*resolved) {
					t.Fatalf("container.Inject() resolved = %v want %v", len(*resolved), len(tt.wantDeps))
				}
				for i := 0; i < len(tt.wantDeps); i++ {
					if !reflect.DeepEqual((*resolved)[i], tt.wantDeps[i]) {
						t.Fatalf("container.Inject() resolved = %v want %v", (*resolved)[i], tt.wantDeps[i])
					}
				}

			} else {
				if !reflect.DeepEqual(tt.wantStruct, tt.args.target) {
					t.Fatalf("container.Inject() target = %v want %v", tt.wantStruct, tt.args.target)
				}
			}
		})
	}
}

func Test_Subcontainer_GetService(t *testing.T) {
	type A struct {
		msg string
	}

	type reg struct {
		svc interface{}
		scp Scope
	}

	type regn struct {
		reg
		name string
	}

	type args struct {
		reg      []interface{}
		regSub   []interface{}
		resolveP interface{}
		resolveS interface{}
		ctx      context.Context
	}

	tests := []struct {
		name       string
		args       args
		wantPErr   bool
		wantSErr   bool
		wantParent interface{}
		wantSub    interface{}
	}{
		{
			name: "Subcontainer resolves different instance",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (*A, error) { return &A{"bb"}, nil }, scp: RequestScope()},
				},
				resolveP: (*A)(nil),
				resolveS: (*A)(nil),
				ctx:      context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &A{"aa"},
			wantSub:    &A{"bb"},
		},
		{
			name: "Subcontainer resolves different instance with parent dependency",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: testB{m: "dep"}, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func(b testB) (*A, error) { return &A{b.m}, nil }, scp: RequestScope()},
				},
				resolveP: (*A)(nil),
				resolveS: (*A)(nil),
				ctx:      context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &A{"aa"},
			wantSub:    &A{"dep"},
		},
		{
			name: "Subcontainer resolves missing interface dependency from parent dependency",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func() (testInterfaceB, error) { return testB{m: "dep"}, nil }, scp: RequestScope()},
				},
				resolveP: (*A)(nil),
				resolveS: (*testInterfaceB)(nil),
				ctx:      context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &A{"aa"},
			wantSub:    testB{"dep"},
		},
		{
			name: "Subcontainer resolves missing struct dependency from parent dependency",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func() (testInterfaceB, error) { return testB{m: "dep"}, nil }, scp: RequestScope()},
				},
				resolveP: (*A)(nil),
				resolveS: (*A)(nil),
				ctx:      context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &A{"aa"},
			wantSub:    &A{"aa"},
		},
		{
			name: "Subcontainer resolves dependency not found",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func() (testInterfaceB, error) { return testB{m: "dep"}, nil }, scp: RequestScope()},
				},
				resolveP: (*A)(nil),
				resolveS: (*testInterfaceA)(nil),
				ctx:      context.Background(),
			},
			wantPErr:   false,
			wantSErr:   true,
			wantParent: &A{"aa"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parentB := newBuilder()
			subB := parentB.NewBuilder()

			for _, r := range tt.args.reg {
				switch t := r.(type) {
				case reg:
					parentB.Register(t.svc, t.scp)
				case regn:
					parentB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}
			for _, r := range tt.args.regSub {
				switch t := r.(type) {
				case reg:
					subB.Register(t.svc, t.scp)
				case regn:
					subB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}

			parentC, err := parentB.Build()
			if err != nil {
				t.Fatalf("Building parent container should not return an error = %v", err)
			}
			subC, err := subB.Build()
			if err != nil {
				t.Fatalf("Building sub container should not return an error = %v", err)
			}

			gotParent, _, err := parentC.GetService(tt.args.resolveP, tt.args.ctx)
			if (err != nil) != tt.wantPErr {
				t.Fatalf("container.GetService() error = %v want %v -> %v", err != nil, tt.wantPErr, err)
			}
			if !reflect.DeepEqual(tt.wantParent, gotParent) {
				t.Fatalf("container.Inject() gotParent = %v want %v", gotParent, tt.wantParent)
			}

			gotSub, _, err := subC.GetService(tt.args.resolveS, tt.args.ctx)
			if (err != nil) != tt.wantSErr {
				t.Fatalf("container.GetService() error = %v want %v -> %v", err != nil, tt.wantSErr, err)
			}
			if !reflect.DeepEqual(tt.wantSub, gotSub) {
				t.Fatalf("container.Inject() gotSub = %v want %v", gotSub, tt.wantSub)
			}

		})
	}
}

func Test_Subcontainer_Inject(t *testing.T) {
	type (
		A struct {
			msg string
		}

		TargetA struct {
			AA A
			BB testInterfaceB `di:"ignore"`
		}

		TargetB struct {
			testG
			A A `di:"ignore"`
			B testInterfaceB
		}

		reg struct {
			svc interface{}
			scp Scope
		}

		regn struct {
			reg
			name string
		}

		args struct {
			reg     []interface{}
			regSub  []interface{}
			injectP interface{}
			injectS interface{}
			ctx     context.Context
		}
	)

	tests := []struct {
		name       string
		args       args
		wantPErr   bool
		wantSErr   bool
		wantParent interface{}
		wantSub    interface{}
	}{
		{
			name: "Subcontainer resolves different instance",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (*A, error) { return &A{"bb"}, nil }, scp: RequestScope()},
				},
				injectP: &TargetA{},
				injectS: &TargetA{},
				ctx:     context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &TargetA{AA: A{"aa"}},
			wantSub:    &TargetA{AA: A{"bb"}},
		},
		{
			name: "Subcontainer resolves different instance with parent dependency",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func(a A) (testInterfaceB, error) { return &testB{m: string(a.msg + " bb")}, nil }, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (testA, error) { return testA{a: 42}, nil }, scp: RequestScope()},
					reg{svc: func(b testInterfaceB, a testA) (*A, error) { return &A{msg: b.Say() + strconv.Itoa(a.a)}, nil }, scp: RequestScope()},
				},
				injectP: &TargetA{},
				injectS: &TargetA{},
				ctx:     context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &TargetA{AA: A{"aa"}},
			wantSub:    &TargetA{AA: A{"aa bb42"}},
		},
		{
			name: "Subcontainer injects into nested struct with parent dependencies",
			args: args{
				reg: []interface{}{
					regn{name: "test", reg: reg{svc: func(b testB) (testA, error) { return testA{a: len(b.m)}, nil }, scp: ContainerScope()}},
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: testB{"BB"}, scp: TransientScope()},
					reg{svc: func(a A) (testInterfaceB, error) { return &testB{m: string(a.msg + " bb")}, nil }, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (testA, error) { return testA{a: 42}, nil }, scp: RequestScope()},
					reg{svc: func(b testInterfaceB, a testA) (*A, error) { return &A{msg: b.Say() + strconv.Itoa(a.a)}, nil }, scp: RequestScope()},
				},
				injectP: &TargetA{},
				injectS: &TargetB{},
				ctx:     context.Background(),
			},
			wantPErr:   false,
			wantSErr:   false,
			wantParent: &TargetA{AA: A{"aa"}},
			wantSub: &TargetB{testG: testG{
				Num: 42,
				msg: "BB",
				aa:  testA{42},
				BB:  testA{4},
			},
				A: A{},
				B: testB{"aa bb"},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parentB := newBuilder()
			subB := parentB.NewBuilder()

			for _, r := range tt.args.reg {
				switch t := r.(type) {
				case reg:
					parentB.Register(t.svc, t.scp)
				case regn:
					parentB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}
			for _, r := range tt.args.regSub {
				switch t := r.(type) {
				case reg:
					subB.Register(t.svc, t.scp)
				case regn:
					subB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}

			parentC, err := parentB.Build()
			if err != nil {
				t.Fatalf("Building parent container should not return an error = %v", err)
			}
			subC, err := subB.Build()
			if err != nil {
				t.Fatalf("Building sub container should not return an error = %v", err)
			}

			_, err = parentC.Inject(tt.args.injectP, tt.args.ctx)
			if (err != nil) != tt.wantPErr {
				t.Fatalf("container.Inject() parent error = %v want %v -> %v", err != nil, tt.wantPErr, err)
			}
			if !reflect.DeepEqual(tt.wantParent, tt.args.injectP) {
				t.Fatalf("container.Inject() gotParent = %v want %v", tt.args.injectP, tt.wantParent)
			}

			_, err = subC.Inject(tt.args.injectS, tt.args.ctx)
			if (err != nil) != tt.wantSErr {
				t.Fatalf("container.Inject() sub error = %v want %v -> %v", err != nil, tt.wantSErr, err)
			}
			if !reflect.DeepEqual(tt.wantSub, tt.args.injectS) {
				t.Fatalf("container.Inject() gotSub = %v want %v", tt.args.injectS, tt.wantSub)
			}

		})
	}
}

func Test_Subcontainer_HandlerFunc(t *testing.T) {
	type (
		A struct {
			msg string
		}

		B struct {
			num int
		}

		reg struct {
			svc interface{}
			scp Scope
		}

		regn struct {
			reg
			name string
		}

		args struct {
			reg     []interface{}
			regSub  []interface{}
			handleP InjectionHandler
			handleS InjectionHandler
		}
	)
	gotP := &[]interface{}{}
	gotS := &[]interface{}{}

	tests := []struct {
		name       string
		args       args
		wantParent []interface{}
		wantSub    []interface{}
		wantPanic  bool
	}{
		{
			name: "Parent and sub resolves different values",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func() (B, error) { return B{42}, nil }, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func(b *B) (A, error) { return A{"cc" + strconv.Itoa(b.num)}, nil }, scp: RequestScope()},
				},
				handleP: func(rw http.ResponseWriter, req *http.Request, a *A) { *gotP = append(*gotP, a) },
				handleS: func(rw http.ResponseWriter, req *http.Request, a *A) { *gotS = append(*gotS, a) },
			},
			wantParent: []interface{}{&A{"aa"}},
			wantSub:    []interface{}{&A{"cc42"}},
		},
		{
			name: "Parent and sub resolves dependency in parent",
			args: args{
				reg: []interface{}{
					reg{svc: A{"aa"}, scp: RequestScope()},
					reg{svc: func() (*B, error) { return &B{42}, nil }, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (B, error) { return B{21}, nil }, scp: RequestScope()},
				},
				handleP: func(rw http.ResponseWriter, req *http.Request, b B) { *gotP = append(*gotP, b) },
				handleS: func(rw http.ResponseWriter, req *http.Request, a A) { *gotS = append(*gotS, a) },
			},
			wantParent: []interface{}{B{42}},
			wantSub:    []interface{}{A{"aa"}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotP = &[]interface{}{}
			gotS = &[]interface{}{}
			parentB := newBuilder()
			subB := parentB.NewBuilder()

			for _, r := range tt.args.reg {
				switch t := r.(type) {
				case reg:
					parentB.Register(t.svc, t.scp)
				case regn:
					parentB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}
			for _, r := range tt.args.regSub {
				switch t := r.(type) {
				case reg:
					subB.Register(t.svc, t.scp)
				case regn:
					subB.RegisterNamed(t.name, t.svc, t.scp)
				}
			}

			parentC, err := parentB.Build()
			if err != nil {
				t.Fatalf("Building parent container should not return an error = %v", err)
			}
			subC, err := subB.Build()
			if err != nil {
				t.Fatalf("Building sub container should not return an error = %v", err)
			}

			defer func() {
				if r := recover(); r != nil && !tt.wantPanic {
					t.Fatalf("container.HandlerFunc() panics = %v, wantPanic %v -> %v", r, tt.wantPanic, string(debug.Stack()))
				}
			}()
			req, _ := http.NewRequest("GET", "/test", nil)
			rr := httptest.NewRecorder()

			handlerP := parentC.HandlerFunc(tt.args.handleP)
			handlerP.ServeHTTP(rr, req)

			if len(*gotP) != len(tt.wantParent) {
				t.Fatalf("container.Inject() gotP len = %v want %v", len(*gotP), len(tt.wantParent))
			}

			for i, item := range tt.wantParent {
				if !reflect.DeepEqual(item, (*gotP)[i]) {
					t.Fatalf("container.Inject() gotParent = %v want %v", (*gotP)[i], item)
				}
			}

			handlerS := subC.HandlerFunc(tt.args.handleS)
			handlerS.ServeHTTP(rr, req)

			if len(*gotS) != len(tt.wantSub) {
				t.Fatalf("container.Inject() gotS len = %v want %v", len(*gotS), len(tt.wantSub))
			}

			for i, item := range tt.wantSub {
				if !reflect.DeepEqual(item, (*gotS)[i]) {
					t.Fatalf("container.Inject() gotSub = %v want %v", (*gotS)[i], item)
				}
			}
		})
	}
}

func Test_Subcontainer_Scopes_GetService(t *testing.T) {
	type (
		reg struct {
			svc interface{}
			scp Scope
		}

		regn struct {
			reg
			name string
		}

		args struct {
			reg      []interface{}
			regSub   []interface{}
			resolveP interface{}
			resolveS interface{}
		}
	)
	parentNum := new(int)
	subNum := new(int)
	tests := []struct {
		name            string
		args            args
		wantParent      interface{}
		wantSub         interface{}
		wantParentWoCtx interface{}
		wantSubWoCtx    interface{}
		wantPErr        bool
		wantSErr        bool
	}{
		{
			name: "Transient Scope",
			args: args{
				reg: []interface{}{
					reg{svc: func() (testA, error) { *parentNum++; return testA{a: *parentNum}, nil }, scp: TransientScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (testA, error) { *subNum++; return testA{a: *subNum}, nil }, scp: TransientScope()},
				},
				resolveP: (*testA)(nil),
				resolveS: (*testA)(nil),
			},
			wantParent:      &testA{2},
			wantParentWoCtx: &testA{3},
			wantSub:         &testA{12},
			wantSubWoCtx:    &testA{13},
		},
		{
			name: "Request Scope",
			args: args{
				reg: []interface{}{
					reg{svc: func() (testA, error) { *parentNum++; return testA{a: *parentNum}, nil }, scp: RequestScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (testA, error) { *subNum++; return testA{a: *subNum}, nil }, scp: RequestScope()},
				},
				resolveP: (*testA)(nil),
				resolveS: (*testA)(nil),
			},
			wantParent:      &testA{1},
			wantParentWoCtx: &testA{2},
			wantSub:         &testA{11},
			wantSubWoCtx:    &testA{12},
		},
		{
			name: "Container Scope",
			args: args{
				reg: []interface{}{
					reg{svc: func() (testA, error) { *parentNum++; return testA{a: *parentNum}, nil }, scp: ContainerScope()},
				},
				regSub: []interface{}{
					reg{svc: func() (testA, error) { *subNum++; return testA{a: *subNum}, nil }, scp: ContainerScope()},
				},
				resolveP: (*testA)(nil),
				resolveS: (*testA)(nil),
			},
			wantParent:      &testA{1},
			wantParentWoCtx: &testA{1},
			wantSub:         &testA{11},
			wantSubWoCtx:    &testA{11},
		},
	}

	for _, tt := range tests {
		*subNum = 10
		*parentNum = 0
		parentB := newBuilder()
		subB := parentB.NewBuilder()

		checkErr := func(err error, want bool, mark string) {
			if (err != nil) != want {
				t.Fatalf("container.Inject() %v err = %v want %v -> %v", mark, (err != nil), want, err)
			}
		}

		for _, r := range tt.args.reg {
			switch t := r.(type) {
			case reg:
				parentB.Register(t.svc, t.scp)
			case regn:
				parentB.RegisterNamed(t.name, t.svc, t.scp)
			}
		}
		for _, r := range tt.args.regSub {
			switch t := r.(type) {
			case reg:
				subB.Register(t.svc, t.scp)
			case regn:
				subB.RegisterNamed(t.name, t.svc, t.scp)
			}
		}

		parentC, err := parentB.Build()
		if err != nil {
			t.Fatalf("Building parent container should not return an error = %v", err)
		}
		subC, err := subB.Build()
		if err != nil {
			t.Fatalf("Building sub container should not return an error = %v", err)
		}

		gotP, ctx, err := parentC.GetService(tt.args.resolveP)
		checkErr(err, tt.wantPErr, "parent")
		gotP, _, err = parentC.GetService(tt.args.resolveP, ctx)
		checkErr(err, false, "parent")

		if !reflect.DeepEqual(gotP, tt.wantParent) {
			t.Fatalf("container.Inject() parent = %v want %v ", gotP, tt.wantParent)
		}

		gotP, _, err = parentC.GetService(tt.args.resolveP)
		checkErr(err, false, "parent")
		if !reflect.DeepEqual(gotP, tt.wantParentWoCtx) {
			t.Fatalf("container.Inject() parent w/o ctx = %v want %v ", gotP, tt.wantParentWoCtx)
		}

		gotS, ctx, err := subC.GetService(tt.args.resolveS)
		checkErr(err, tt.wantSErr, "sub")
		gotS, _, err = subC.GetService(tt.args.resolveS, ctx)
		checkErr(err, false, "sub")

		if !reflect.DeepEqual(gotS, tt.wantSub) {
			t.Fatalf("container.Inject() sub = %v want %v ", gotS, tt.wantSub)
		}

		gotS, _, err = subC.GetService(tt.args.resolveS)
		checkErr(err, false, "sub")
		if !reflect.DeepEqual(gotS, tt.wantSubWoCtx) {
			t.Fatalf("container.Inject() sub = %v want %v ", gotS, tt.wantSubWoCtx)
		}

	}
}
