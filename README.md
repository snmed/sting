# Sting #

__Current Version:__ 1.0.0

*Sting* is a lightweight dependency injection for go. It uses reflection to build a di container and to resolve dependencies. Therefore it comes with a performance penalty in comparsion to a non *sting* based application.

[![GoDoc](https://godoc.org/bitbucket.org/snmed/sting?status.svg)](https://godoc.org/bitbucket.org/snmed/sting)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/snmed/sting)](https://goreportcard.com/report/bitbucket.org/snmed/sting)

## Table of content ##
- [Features](#markdown-header-features)
- [Usage](#markdown-header-usage)
  - [Supported Types](#markdown-header-supported-types)
  - [Register Services](#markdown-header-register-services)
    - [Register](#markdown-header-register)
    - [RegisterNamed](#markdown-header-registernamed)
  - [Retrieve Services](#markdown-header-retrieve-services)
    - [GetService](#markdown-header-getservice)
    - [Inject](#markdown-header-inject)
    - [HandlerFunc](#markdown-header-handlerfunc)
  - [Creating a child builder](#markdown-header-creating-a-child-builder)
- [License](#markdown-header-license)


## Features ##
- Easy service registration
- No complex configuration needed
- No external dependencies
- Supports named services
- Supports sub containers
- Supports function and struct injection
- Supports resolving services by name, interface and struct
- Supports http.Handler injection by using a sting.HandlerFunc
- Provides three different service lifetimes (transient, request, container)
- Supports custom lifetimes

## Usage ##

### Supported Types ###
The following types are supported for service registration and resolving:

- struct
- pointer to struct
- interface

### Register Services ###
Create a builder and register services, afterwards a call to `Build` builds the dependency graph and verifies that no cycles or lifetime violation exists. To register a service use `builder.Register` or `builder.RegisterNamed`, the latter one register a service with a name, this is useful if one wants to register the same type more then once.
```go
package main

import (
    "fmt"

    "bitbucket.org/snmed/sting"
)

type UsefulStruct struct { msg string }
type UsefulInterface interface {
    Say()
}

func (u UsefulStruct) Say() {
    fmt.Println(u.msg)
}

func main(){
    // Register services and build container at once
    container, err := sting.NewBuilder().
        Register(UsefulStruct{"Very useful"}, sting.TransientScope()).
        Register( // A creator function
            func(u UsefulStruct) (UsefulInterface, error) {
                // Do some useful initialisation work here
                 return u,nil },
            sting.ContainerScope(),
        ).
        RegisterNamed("alsoUseful",&UsefulStruct{"More useful"}, sting.TransientScope()).
        Build()

        svc, _, err := container.GetService((*UsefulInterface)(nil))

        if err == nil {
            svc.(*UsefulInterface).Say()
        }

}

```
If a creator function is used, it must be in form of:
```go
// creatorFunc
func([arg type]*) (type, error) { /* Do some initialisation work */ }
```
The creator function can have zero or more arguments of a [supported type](#markdown-header-supported-types). This dependencies will be resolved while building the di container and returns an error if a required item is not registered. The builder deduces the type that will be created by the creator function and register it. As for the arguments, the first return parameter must be of a [supported type](#markdown-header-supported-types).

#### Register ####
Input:

1. `ARG1 := creatorFunc | struct | *struct` service to register.
2. `ARG2 := sting.Scope` a lifetime scope to use for this service.

Output:

1. `RET := sting.Builder` the used Builder, to support fluent registration of services.
```go
// Signature
builder.Register(interface{}, Scope) Builder
// Example
builder.Register(UsefulStruct{msg: "Very Useful"}, sting.TransientScope())
```
#### RegisterNamed ####
Input:

1. `ARG1 := string` the name under which the service will be registered.
2. `ARG2 := creatorFunc | struct | *struct` service to register.
3. `ARG3 := sting.Scope` a lifetime scope to use for this service.

Output:

1. `RET := sting.Builder` the used Builder, to support fluent registration of services.
```go
// Signature
builder.RegisterNamed(string, interface{}, Scope) Builder
// Example
builder.RegisterNamed("MyUswfulService", UsefulStruct{msg: "Very Useful"}, sting.TransientScope())
```
### Retrieve Services ###
To retrieve a service use `GetService`, `Inject` or `HandlerFunc`. Each of these function has a different use case as described in the following paragraphs.

#### GetService ####
Input:

1. `ARG1 := (*interface)(nil) | (*struct)(nil) | string` nil pointer to a service type or a string for a named service. 
2. `ARG2 := context.Context` context to use for request scoped services.

Returns:

1. `RET1 := interface{} | nil` returns the requested service or nil.
2. `RET2 := context.Context` returns a context for request scoped services. Further calls to `Inject` or `GetService` should use this context
3. `RET3 := error` returns an error if a service is not found or could not be created, as well as when an invalid type is passed to `ARG1`.
```go
// Signature
container.GetService(interface{}, ...context.Context) (interface{}, context.Context, error)
// Example
svc, ctx, err := container.GetService((*UsefulInterface)(nil), someContext)
```
`GetService` takes at least one argument of type `(*interface)(nil)`, `(*struct)(nil)` or a `string` and one optional argument of type `context.Context`. If more than one context is passed, only the first one is used all other contexts will be ignored. Context is only required if a request scope is used and therefore it is necessary to pass the returned context to any further call to `GetService` or `Inject` within related requests. This is the most straight forward way to get a single service and returns always a pointer to struct or a interface.

#### Inject ####
Input:

1. `ARG1 := *struct | func([arg type]*) ` a pointer to struct or a function into which the dependencies will be injected. 
2. `ARG2 := context.Context` context to use for request scoped services.

Returns:

1. `RET1 := context.Context` returns a context for request scoped services. Further calls to `Inject` or `GetService` should use this context
2. `RET2 := error` returns an error if a service is not found or could not be created, as well as when an invalid type is passed to `ARG1`.
```go
// Signature
container.Inject(interface{}, ...context.Context) (context.Context, error)
// Example with stuct
svc, ctx, err := container.Inject(&myController{}, someContext)
// Example with a function 
svc, ctx, err := container.Inject(func(cfg Config, repo Repository, log *Logger) { /* Do something with the dependencies */ }, someContext)
```
`Inject` takes a pointer to struct or a function as first argument and an optional second argument of type `context.Context`. As for `GetService` only the first context is used. A function can have as many argument as it needs, but all of them must be registered within the di container, otherwise an error is returned. Arguments must be of a [supported type](#markdown-header-supported-types). If a pointer to struct is used as first argument, the di container tries to resolve every [supported type](#markdown-header-supported-types) defined in that struct and inject the services into it. The following tags can be used to change the behavior of the di container:
```go
type target struct {
    repo PersonRepository `di:"ignore"` // This field will be ignored, must be specified for every struct, *struct and interface which should not be resolved.
    Cfg Config `di:"name=myconfig"` // This field wants a named service of type Config.
    msg string `di:"func=SetMessage"` // This field has a setter with a dependecy, the di container calls the setter with all required arguments (must be registered)
}
```
If a struct has unexported fields of [supported types](#markdown-header-supported-types), the di container tries to find a setter function like `Set<UpperCaseFieldName>` and tries to inject it, otherwise `Inject` returns an error. It is possible to combine the two tags `name` and `func` to use a named service together with a setter function. Setter functions must have one argument of a [supported type](#markdown-header-supported-types), the di container deduces the required dependency according the setters argument type. For example `SetMessage(c Config)` looks for a service of type `Config`, if not found an error is returned.

`Inject` accepts also functions in form of 
```go
func([arg type]*) { /* use dependency */ }
```
it can have one or more arguments with a [supported type](#markdown-header-supported-types).
 
#### HandlerFunc ####
Input:

1. `ARG1 := InjectionHandler` a http.HandlerFunc with additional arguments of a [supported type](#markdown-header-supported-types). 

Returns:

1. `RET1 := http.Handler` returns http.Handler, panics if a dependency is missing, an invalid type as argument passed or an error occured while creating a dependency.

```go
// Signature
container.HandlerFunc(handler InjectionHandler) http.Handler
// Example
container.HandlerFunc(func(w http.ResponseWriter, r *http.Request, repo PersonRepository, pers Person){ /* Do something useful */ })

```
`HandlerFunc` takes an `InjectionHandler` as argument and returns a http.Handler. It can be used to inject dependencies into a function which serves http requests. It's not necessary to pass any context to use a request scope. 

### Creating a child builder ###
If a use case requires the same type of dependencies more than once and a named service is not an option, then creating a child builder is a possible solution.
```go
// Signature
builder.NewBuilder() Builder
// Example
builder.Register(Person{}, sting.TransientScope()
childBuilder.Register(Person{}, sting.TransientScope()

container, perr := builder.Build()
subcontainer, serr := childBuilder.Build()
```
`NewBuilder` returns a new Builder with no registered services. If a child Builder can't find a dependecies, it will call `GetService` on the parent Builder to obtain a service. __Caveat__: A child builder must be built before his parent, otherwise `Build` returns with an error. This is necessary for checking missing dependencies and lifetime violations. 


# License #
[BSD-3-Clause](./LICENSE)