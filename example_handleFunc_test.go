package sting_test

import (
	"fmt"
	"log"
	"net/http/httptest"

	"bitbucket.org/snmed/sting"
)

// This example shows how to use the di container in a web application or a microservice.
// Wrap any InjectionHandler inside a container.HandlerFunc. A InjectionHandler is in form
// of a http.Handler, but can have as many additional arguments as needed, as far as the argument
// types are one of the supported di types.
func Example_handlerFunc() {

	container, err := sting.NewBuilder().
		// Register a pointer to struct with a  container scope
		Register(&Config{DefaultId: 42, DefaultName: "Judge Dredd"}, sting.RequestScope()).
		// Register a creator function with a dependency and container scope
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: c.DefaultId, name: c.DefaultName}, nil
			},
			sting.RequestScope(),
		).
		// Register a named service with an already registered type
		Register(
			func() (*Person, error) { return &Person{Id: 11, Name: "Selene"}, nil },
			sting.RequestScope(),
		).
		Build()

	if err != nil {
		log.Fatalln(err)
	}

	handler := ChangeCfgMiddleware(container, container.HandlerFunc(FindPerson))
	handler = LogMiddleware(handler)

	recorder := httptest.NewRecorder()
	handler.ServeHTTP(recorder, httptest.NewRequest("GET", "/api?id=55", nil))

	fmt.Println(recorder.Body)

	recorder = httptest.NewRecorder()
	handler.ServeHTTP(recorder, httptest.NewRequest("GET", "/api", nil))

	fmt.Println(recorder.Body)

	// Output: Calling URL /api?id=55 with method GET ...
	// &{55 Pumuckel}
	// Calling URL /api with method GET ...
	// {11 Selene}

}
