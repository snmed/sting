package sting

import (
	"context"
	"fmt"
	"reflect"
)

const (
	containerCtxKey = stingCtxKey("ContainerScope")
	requestCtxKey   = stingCtxKey("RequestScope")
)

var (
	transientScope = &scope{}
	reqScope       = &requestScope{contextScope{key: requestCtxKey}}
	contScope      = &containerScope{contextScope{key: containerCtxKey}}
)

// ScopeLifeTimeFunc returns the life time level of a scope. Dependencies defined within a certain level
// must not have any dependencies defined in a higher level.
type ScopeLifeTimeFunc func(Scope) int

// DefaultScopeLifeTimeFunc is the default ScopeLifeTimeFunc for a Builder instance.
func DefaultScopeLifeTimeFunc(s Scope) int {
	switch s.(type) {
	case *requestScope:
		return 20
	case *containerScope, *scope:
		return 10
	default:
		return 30
	}
}

type stingCtxKey string
type scopeCtx struct {
	items map[interface{}]interface{}
}

// Scope provides a storage for dependency objects and manages
// their lifetime.
type Scope interface {
	Get(context.Context, interface{}, ActionFunc) (interface{}, error)
}

//type scope func(context.Context, interface{}, actionFunc) (interface{}, error)
type scope struct{}

func (s scope) String() string {
	return "Transient Scope"
}

func (s scope) Get(ctx context.Context, t interface{}, action ActionFunc) (interface{}, error) {
	return action(ctx)
}

// TransientScope executes the ActionFunc for each call and returns
// the created instance.
func TransientScope() Scope {
	return transientScope
}

type containerScope struct {
	contextScope
}

func (c containerScope) String() string {
	return "Container Scope"
}

type requestScope struct {
	contextScope
}

func (c requestScope) String() string {
	return "Request Scope"
}

type contextScope struct {
	key stingCtxKey
}

func (s contextScope) Get(ctx context.Context, t interface{}, action ActionFunc) (interface{}, error) {
	sctx := ctx.Value(s.key)
	// If there is no scope context, fallback to a transient scope behaviour.
	if sctx == nil {
		return action(ctx)
	}
	scp := sctx.(*scopeCtx)
	err := resolveAndStore(ctx, scp, t, action)
	return scp.items[t], err
}

func resolveAndStore(ctx context.Context, scp *scopeCtx, t interface{}, action ActionFunc) error {
	item, ok := scp.items[t]
	if item != nil && ok {
		return nil
	}

	obj, err := action(ctx)
	if err != nil {
		return err
	}

	scp.items[t] = obj
	return nil
}

// ContainerScope executes on first call the ActionFunc and
// returns the same instance for all subsequent calls.
func ContainerScope() Scope {
	return contScope
}

// RequestScope executes on first call the ActionFunc and returns
// for all subsequent calls in the same request the created instance.
func RequestScope() Scope {
	return reqScope
}

// The Builder interface is responsible to register dependencies and
// to build the di container.
type Builder interface {
	// Register registers a creator function and deduces its service name with
	// reflection from the first output parameter of the creator function. A creator
	// function has the form:
	//
	//	func((arg type)*) (type, error) { /* construct return type */ }
	//
	// The function can have zero or more arguments, but only two return types whereby the
	// last one must be the error interface. The 'type' must be one of struct, pointer to struct or
	// interface. It's also possible to register a struct or pointer to struct directly, see examples
	// for more information. Caveat: It is not possible to register a pointer to struct and a struct
	// of the same type, this might be changed in a future release.
	Register(interface{}, Scope) Builder
	// RegisterNamed registers a given creator function with a specified name and scope.
	// It's possible to register the same type with different names. Also it's possible to register
	// a type that is already registered with the 'Register' function. It's also possible
	// to register a struct or pointer to struct directly, see examples for more information.
	RegisterNamed(string, interface{}, Scope) Builder
	// Build builds and verifies all registered services. It returns an error if any of
	// this cases occurs:
	//
	//  - Invalid creator function passed
	//  - Service already registered
	//  - Missing dependency for creator function
	//  - Container already built
	//  - Parent container not built
	//  - If creator function uses dependency with a lower life time level
	Build() (Container, error)

	// NewBuilder returns a new child builder, so you can overwrite some
	// dependencies in its parent builder or register additional dependencies.
	// It is necesseray to build a parent container first, before building the child
	// container. The child container lookup dependencies first in its own registry,
	// if it can't find a requested service or dependency, it will ask its parent container
	// for any missing item by calling GetService on it.
	NewBuilder() Builder
}

// Ensure all interface receiver are implemented.
var _ Builder = &builder{}

// builder is responsible to register and build dependency objects.
type builder struct {
	built         bool
	container     *container
	errors        errors
	options       Options
	parent        *builder
	registrations depMap
}

// Configure changes options of the Builder instance.
type Configure func(*Options)

// Options is used to configure the Builder instance.
type Options struct {
	ignoreScopeCheck bool
	lifeTimeFunc     ScopeLifeTimeFunc
}

// namedSvcKey serves as map index for named services
type namedSvcKey string

// Register registers a creator function and deduces its service name with
// reflection from the first output parameter of the creator function. A creator
// function has the form:
//
//	func((arg type)*) (type, error) { /* construct return type */ }
//
// The function can have zero or more arguments, but only two return types whereby the
// last one must be the error interface. The 'type' must be one of struct, pointer to struct or
// interface. It's also possible to register a struct or pointer to struct directly, see examples
// for more information.
func (b *builder) Register(creatorFunc interface{}, scope Scope) Builder {
	b.registerService("", creatorFunc, scope)
	return b
}

// RegisterNamed registers a given creator function with a specified name and scope.
// It's possible to register the same type with different names. Also it's possible to register
// a type that is already registered with the 'Register' function. It's also possible
// to register a struct or pointer to struct directly, see examples for more information.
func (b *builder) RegisterNamed(name string, creatorFunc interface{}, scope Scope) Builder {
	b.registerService(name, creatorFunc, scope)
	return b
}

// Build builds and verifies all registered services. It returns an error if any of
// this cases occurs:
//
//  - Invalid creator function passed
//  - Service already registered
//  - Missing dependency for creator function
//  - Container already built
//  - Parent container not built
//  - If creator function uses dependency with a lower life time level
func (b *builder) Build() (Container, error) {
	// If there is a parent builder, it must be build first
	if b.parent != nil && !b.parent.built {
		return nil, serr{
			err: errParentContainerNotBuilt,
			msg: "Parent container must be built first",
		}

	}

	if b.built {
		return b.container, nil
	}

	err := b.errors.errorOrNil()
	if err != nil && !b.built {
		return nil, err
	}

	// Build the dependency graph
	b.container.registry = make(depMap, 0)
	services := b.registrations.clone()
	for k, v := range services {
		delete(services, k)
		if err := b.buildGraph(v, services); err != nil {
			return nil, err
		}
	}

	for _, n := range b.container.registry {
		if err := checkCycle(n, []*depNode{}); err != nil {
			return nil, err
		}
	}

	b.buildScopeFuncs()

	b.built = true
	return b.container, nil
}

// buildScopeFuncs creates for every depNode an actionFunc.
func (b *builder) buildScopeFuncs() {
	for _, v := range b.container.registry {
		// Service has no dependencies so we
		// create a simple actionFunc.
		if len(v.dependencies) == 0 {
			v.scopeFunc = createSimpleFunc(v)
			continue
		}
		// Node has some dependencies so we
		// create a little less simple actionFunc.
		v.scopeFunc = createComplexFunc(b.container.parent, v)
	}
}

// createCompFunc creates a complex actionFunc for a creatorFunc with parameters.
func createComplexFunc(c Container, n *depNode) ActionFunc {
	funcs := []*ActionFunc{}
	for i, d := range n.dependencies {
		idx := i // Only the fool use i in a closure ;-)
		// If d is nil then get dependency from parent container
		if d == nil {
			fn := ActionFunc(func(ctx context.Context) (interface{}, error) {
				svc, _, err := c.GetService(ensureGetServiceArg(n.parameters[idx]), ctx)
				return svc, err
			})
			funcs = append(funcs, &fn)
			continue
		}
		// So it's a dependency handled by the current container
		funcs = append(funcs, &d.scopeFunc)
	}

	return func(ctx context.Context) (interface{}, error) {
		in := []reflect.Value{}
		for idx, f := range funcs {

			svc, err := (*f)(ctx)
			if err != nil {
				return nil, err
			}
			// If svc is nil, creatorFunc produces nil values
			// without an error and therefore it must be a configuration error
			if svc == nil {
				return nil, serr{
					msg: fmt.Sprintf("CreatorFunc for type '%v' produces nil value without error", n.produces),
					err: errActionFuncReturnedNil,
				}
			}

			in = append(in, getInputParam(n.parameters[idx], svc))
		}

		fn := reflect.ValueOf(n.creatorFunc)
		result := fn.Call(in)

		// Assert to error without panic
		err, _ := result[1].Interface().(error)
		return result[0].Interface(), err
	}
}

// If we have a pointer to struct, we need a pointer to
// struct and not a pointer to pointer to struct.
func ensureGetServiceArg(t reflect.Type) interface{} {
	if t.Kind() == reflect.Ptr {
		return reflect.New(t.Elem()).Interface()
	}
	return reflect.New(t).Interface()
}

// getInputParam checks what kind of input parameter is required and
// transforms the current service to that kind.
func getInputParam(required reflect.Type, current interface{}) reflect.Value {
	wantsPtr := required.Kind() == reflect.Ptr
	svcT := reflect.TypeOf(current)

	// current is exactly that what required needs
	if (svcT.Kind() == reflect.Ptr && wantsPtr) ||
		(svcT.Kind() != reflect.Ptr && !wantsPtr) {
		return reflect.ValueOf(current)
	}

	// current is not a ptr but required needs one
	if wantsPtr && svcT.Kind() != reflect.Ptr {
		ptr := reflect.New(svcT)
		ptr.Elem().Set(reflect.ValueOf(current))
		return ptr
	}

	// current is ptr but we need not a ptr
	return reflect.ValueOf(current).Elem()
}

// createSimpleFunc creates a simple actionFunc for a creatorFunc without
// parameters or for a struct or pointer to struct.
func createSimpleFunc(n *depNode) ActionFunc {
	val := reflect.ValueOf(n.creatorFunc)
	if val.Kind() == reflect.Func {
		return func(ctx context.Context) (interface{}, error) {
			result := val.Call([]reflect.Value{})
			// We know that the creatorFunc must have the same return
			// signature as actionFunc, so no panic should be occur
			// except error is nil so let it check first.
			err, _ := result[1].Interface().(error)
			return result[0].Interface(), err
		}
	}

	// It's a struct or a struct pointer
	return func(ctx context.Context) (interface{}, error) {
		return n.creatorFunc, nil
	}
}

// checkCycle traverses all dependencies of a depNode and checks if a cycle exists.
// Maybe not the fastest way but for now it's fine.
func checkCycle(n *depNode, processedNodes []*depNode) error {
	// In case n is nil, the dependency is managed by a parent builder.
	// If we came so far, it means that we have already checked if the dependency
	// is available in the parent builder so return a nil error.
	if n == nil {
		return nil
	}
	// Check if node is already processed, if so, we have a cycle in our graph.
	for _, dep := range processedNodes {
		if dep == n {
			return serr{
				err: errCycleDetected,
				msg: fmt.Sprintf("Cycle detected, %v ", formatDepMap(processedNodes, n)),
			}
		}
	}

	processedNodes = append(processedNodes, n)
	for _, dep := range n.dependencies {
		procNodes := make([]*depNode, len(processedNodes))
		copy(procNodes, processedNodes)

		if err := checkCycle(dep, procNodes); err != nil {
			return err
		}
	}

	return nil
}

// buildGraph builds a dependency graph for all registered services.
func (b *builder) buildGraph(n *depNode, services depMap) error {

	// If node has a name, use name as key
	var key interface{} = n.produces
	if n.name != "" {
		key = namedSvcKey(n.name)
	}
	b.container.registry[key] = n

	for _, p := range n.parameters {
		if p.Kind() == reflect.Ptr {
			p = p.Elem()
		}
		dep, ok := b.registrations[p]
		isParent := false

		// Is type in parent builder registered
		if !ok && b.parent != nil {
			dep, ok = b.parent.registrations[p]
			isParent = true
		}

		if !ok {
			return serr{
				err: errMissingDependency,
				msg: fmt.Sprintf("Missing dependency '%v' needs '%v'", reflect.TypeOf(n.creatorFunc), p),
			}
		}

		// Return error if a service with lower scope needs a dependency with a
		// higher level. Request scope should not be injected into a singlton scope.
		if !b.options.ignoreScopeCheck && b.options.lifeTimeFunc(n.scope) < b.options.lifeTimeFunc(dep.scope) {
			return serr{
				err: errLifetimeViolation,
				msg: fmt.Sprintf("Lifetime error: dependency '%v' with scope '%v' needs '%v' with scope '%v'",
					n.produces, n.scope, dep.produces, dep.scope),
			}
		}

		if isParent {
			n.dependencies = append(n.dependencies, nil)
			continue
		}
		n.dependencies = append(n.dependencies, dep)

		// dep is registered within the same builder, now lookup dep's dependencies
		if _, ok := services[p]; !ok {
			continue // This dependency is already processed
		}
		delete(services, p)
		if err := b.buildGraph(dep, services); err != nil {
			return err
		}
	}

	return nil
}

// NewBuilder returns a new child builder. It is posssible to
// register types that are already registered in the parent builder.
// Child builder looks for unknown types in its parent builder, therefore
// the parent builder must be built first.
func (b *builder) NewBuilder() Builder {
	nb := newBuilder()
	nb.container.parent = b.container
	nb.parent = b
	nb.options = b.options
	return nb
}

func (b *builder) registerService(key string, creatorFunc interface{}, scope Scope) {
	if b.built {
		b.errors = errors{
			serr{
				err: errContainerAlreadyBuilt,
				msg: "Container already built can't add service",
			},
		}
		return
	}

	node, err := createDepNode(key, creatorFunc)
	if err != nil {
		b.errors = append(b.errors, err)
		return
	}
	node.scope = scope
	var regKey interface{} = node.produces
	if key != "" {
		regKey = namedSvcKey(key)
	}

	// If a node exists with the same key
	// don't add node but add an error.
	if _, ok := b.registrations[regKey]; ok {
		b.errors = append(b.errors,
			serr{
				err: errServiceAlreadyRegistered,
				msg: fmt.Sprintf("Service with same key %v already added", regKey),
			})
		return
	}

	b.registrations[regKey] = node
}

// createDepNode creates a new dependency node and evaluates the required
// values from the passed creatorFunc. It returns an error if the creatorFunc
// is invalid.
func createDepNode(key string, creatorFunc interface{}) (node *depNode, err error) {
	node = &depNode{parameters: make([]reflect.Type, 0), dependencies: make([]*depNode, 0)}
	node.name = key
	node.creatorFunc = creatorFunc

	if creatorFunc == nil {
		return nil, serr{
			err: errInvalidCreatorFunc,
			msg: "Nil is not a valid value for parameter creatorFunc",
		}
	}

	creatorType := reflect.TypeOf(creatorFunc)
	if !(isSupportedDiType(creatorType) || creatorType.Kind() == reflect.Func) {
		return nil, serr{
			err: errInvalidCreatorFunc,
			msg: fmt.Sprintf("creatorFunc is %v must be of type interface, func, struct or pointer to struct", creatorType),
		}
	}

	// It's a function so evaluate the return type and required parameters
	if creatorType.Kind() == reflect.Func {
		if err := evalCreatorFunc(node, creatorType); err != nil {
			return nil, err
		}
		return node, nil
	}

	if creatorType.Kind() == reflect.Ptr {
		node.produces = creatorType.Elem()
		node.producePtr = true
		return
	}

	node.produces = creatorType
	return
}

// evalCreatorFunc inspects the creatorFunc and populates
// the passed depNote with the required values.
func evalCreatorFunc(node *depNode, funcType reflect.Type) error {
	// First find the type that is being created by this function
	if funcType.NumOut() != 2 {
		return serr{
			err: errInvalidCreatorFunc,
			msg: "Creator function needs two return parameters",
		}
	}
	pType := funcType.Out(0)

	// Check if return type is a interface, struct or pointer to struct
	errFirstParamMsg := "First return value is '%v' but must be of type interface, struct or pointer to struct"
	if !isSupportedDiType(pType) {
		return serr{
			err: errInvalidCreatorFunc,
			msg: fmt.Sprintf(errFirstParamMsg, pType),
		}
	}

	if funcType.Out(1).Kind() != reflect.Interface ||
		!funcType.Out(1).Implements(reflect.TypeOf((*error)(nil)).Elem()) {
		return serr{
			err: errInvalidCreatorFunc,
			msg: fmt.Sprintf("Second return value is '%v' but must be of type error", funcType.Out(1)),
		}
	}

	// Get types of all required arguments
	parameters := make([]reflect.Type, funcType.NumIn())
	for i := 0; i < funcType.NumIn(); i++ {
		arg := funcType.In(i)
		if !isSupportedDiType(arg) {
			return serr{
				err: errInvalidCreatorFunc,
				msg: fmt.Sprintf("Parameter %v has an unsupported type %v", i+1, arg),
			}
		}
		parameters[i] = arg
	}

	// So far no errors, set values.
	node.produces = pType
	if node.producePtr = pType.Kind() == reflect.Ptr; node.producePtr {
		// We know already that Element must be a pointer struct
		node.produces = pType.Elem()
	}
	node.parameters = parameters

	return nil
}

func newBuilder(c ...Configure) *builder {
	opt := Options{ignoreScopeCheck: false, lifeTimeFunc: DefaultScopeLifeTimeFunc}
	for i := range c {
		c[i](&opt)
	}

	return &builder{
		errors:        make(errors, 0),
		built:         false,
		container:     newContainer(),
		options:       opt,
		registrations: make(depMap, 0),
	}
}
