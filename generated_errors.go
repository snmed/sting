// This is an auto-generated file and will be
// overwritten next time go:generate is executed.
// Date: 2017-09-17 23:02:38.511987394 +0200 CEST m=+0.000870160

package sting

// IsInvalidCreatorFunc checks if an error is caused by an invalid creator function.
func IsInvalidCreatorFunc(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errInvalidCreatorFunc
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errInvalidCreatorFunc {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsServiceAlreadyRegistered checks if an error is caused by registering a service twice.
func IsServiceAlreadyRegistered(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errServiceAlreadyRegistered
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errServiceAlreadyRegistered {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsContainerAlreadyBuilt checks if an error is caused by registering a service on an already built container.
func IsContainerAlreadyBuilt(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errContainerAlreadyBuilt
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errContainerAlreadyBuilt {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsParentContainerNotBuilt checks if an error is caused by calling Build on sub container with a not build parent container.
func IsParentContainerNotBuilt(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errParentContainerNotBuilt
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errParentContainerNotBuilt {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsCycleDetected checks if an error is caused by a cycle in the dependency graph.
func IsCycleDetected(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errCycleDetected
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errCycleDetected {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsLifetimeViolation checks if an error is caused by a lifetime violation.
func IsLifetimeViolation(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errLifetimeViolation
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errLifetimeViolation {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsMissingDependency checks if an error is caused by a missing dependency.
func IsMissingDependency(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errMissingDependency
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errMissingDependency {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsActionFuncReturnedNil checks if an error is caused by an action function which returns nil.
func IsActionFuncReturnedNil(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errActionFuncReturnedNil
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errActionFuncReturnedNil {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsInvalidInjectionHandler checks if an error is caused by an invalid injection handler.
func IsInvalidInjectionHandler(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errInvalidInjectionHandler
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errInvalidInjectionHandler {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsServiceNotFound checks if an error is caused by a unresolved dependency.
func IsServiceNotFound(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errServiceNotFound
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errServiceNotFound {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsInvalidServiceRequest checks if an error is caused by a call to GetService with an invalid argument.
func IsInvalidServiceRequest(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errInvalidServiceRequest
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errInvalidServiceRequest {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsInvalidInjectionTarget checks if an error is caused by a call to Inject with an invalid target.
func IsInvalidInjectionTarget(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errInvalidInjectionTarget
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errInvalidInjectionTarget {
				return true
			}
		}
		return false
	default:
		return false
	}
}

// IsUnexpectedError checks if an error is an unexpected error.
func IsUnexpectedError(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == errUnexpectedError
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == errUnexpectedError {
				return true
			}
		}
		return false
	default:
		return false
	}
}
