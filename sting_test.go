package sting

import (
	"reflect"
	"testing"
)

func Test_isSupportedDiType(t *testing.T) {
	type A struct{}

	type args struct {
		t reflect.Type
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Struct is allowed",
			args: args{
				reflect.TypeOf(A{}),
			},
			want: true,
		},
		{
			name: "Struct Ptr is allowed",
			args: args{
				reflect.TypeOf(&A{}),
			},
			want: true,
		},
		{
			name: "Interfaces is allowed",
			args: args{
				reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
			},
			want: true,
		},
		{
			name: "Function is not allowed",
			args: args{
				reflect.TypeOf(func() {}),
			},
			want: false,
		},
		{
			name: "Int is not allowed",
			args: args{
				reflect.TypeOf(42),
			},
			want: false,
		},
		{
			name: "String is not allowed",
			args: args{
				reflect.TypeOf("42"),
			},
			want: false,
		},
		{
			name: "Ptr to interface is not allowed",
			args: args{
				reflect.TypeOf((*testInterfaceA)(nil)),
			},
			want: false,
		},
		{
			name: "Nil is not allowed",
			args: args{
				reflect.TypeOf(nil),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isSupportedDiType(tt.args.t); got != tt.want {
				t.Errorf("isSupportedDiType() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_tagValue(t *testing.T) {
	type args struct {
		key string
		s   string
	}
	tests := []struct {
		name  string
		args  args
		want  bool
		want1 string
	}{
		{
			name: "Find key without value",
			args: args{
				key: "ignore",
				s:   "ignore,func=setRepository,name=svcxyz",
			},
			want:  true,
			want1: "",
		},
		{
			name: "Find key with value",
			args: args{
				key: "func",
				s:   "ignore,func=setRepository,name=svcxyz",
			},
			want:  true,
			want1: "setRepository",
		},
		{
			name: "Find no key and no value",
			args: args{
				key: "test",
				s:   "ignore,func=setRepository,name=svcxyz",
			},
			want:  false,
			want1: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tagValue(tt.args.key, tt.args.s)
			if got != tt.want {
				t.Errorf("getTagValue() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("getTagValue() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

type testC struct {
	num int `di:"func=SetMyInt,name=test"`
}

func (t *testC) SetMyInt(i testInterfaceA) {
	t.num = 42
}

type testD struct {
	num testInterfaceB `di:"func=SetMyInt"`
}

func (t *testD) SetMyInt() {
	t.num = nil
}

type testE struct {
	Num testInterfaceB `di:"func=SetMyInt"`
}

func (t *testE) SetMyInt(f func()) {
	t.Num = nil
}

type testF struct {
	Num testInterfaceB `di:"func=SetMyInt"`
}

func (t *testF) SetMyInt(a *testA) {
	t.Num = nil
}

func Test_extractDiInfo(t *testing.T) {
	const funcName = "SetMyInt"
	type A struct {
		Log  testInterfaceA
		Ta   testA
		Tptr *testB
		num  int
		fn   func() error
	}

	type B struct {
		Log testInterfaceA `di:"name=test"`
	}

	type C struct {
		log testInterfaceA `di:"ignore"`
	}

	type D struct {
		Log testInterfaceA
	}

	type E struct {
		D
		Test testInterfaceB
	}

	type F struct {
		E
		Bla A
	}

	type G struct {
		log testInterfaceA
	}

	type args struct {
		t reflect.Type
	}
	tests := []struct {
		name    string
		args    args
		want    []diFieldCtx
		wantErr bool
	}{
		{
			name: "Extract supported fields without tag",
			args: args{t: reflect.TypeOf(A{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					useFunc: "",
				},
				{
					index:   1,
					regKey:  reflect.TypeOf(testA{}),
					typ:     reflect.TypeOf(testA{}),
					useFunc: "",
				},
				{
					index:   2,
					regKey:  reflect.TypeOf(testB{}),
					typ:     reflect.TypeOf(&testB{}),
					useFunc: "",
				},
			},
			wantErr: false,
		},
		{
			name:    "Do not extract ignored fields",
			args:    args{t: reflect.TypeOf(C{})},
			want:    []diFieldCtx{},
			wantErr: false,
		},
		{
			name: "Extract embedded fields",
			args: args{t: reflect.TypeOf(E{})},
			want: []diFieldCtx{
				{
					index:   0,
					useFunc: "",
					embeddedFields: []diFieldCtx{
						{
							index:   0,
							regKey:  reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
							typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
							useFunc: "",
						},
					},
				},
				{
					index:   1,
					regKey:  reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					typ:     reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					useFunc: "",
				},
			},
			wantErr: false,
		},
		{
			name: "Extract nested embedded fields",
			args: args{t: reflect.TypeOf(F{})},
			want: []diFieldCtx{
				{
					index:   0,
					useFunc: "",
					embeddedFields: []diFieldCtx{
						{
							embeddedFields: []diFieldCtx{
								{
									index:   0,
									regKey:  reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
									typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
									useFunc: "",
								},
							},
						},

						{
							index:   1,
							regKey:  reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
							typ:     reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
							useFunc: "",
						},
					},
				},
				{
					index:   1,
					regKey:  reflect.TypeOf(A{}),
					typ:     reflect.TypeOf(A{}),
					useFunc: "",
				},
			},
			wantErr: false,
		},
		{
			name: "Extract field with name tag",
			args: args{t: reflect.TypeOf(B{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  "test",
					typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					useFunc: "",
				},
			},
			wantErr: false,
		},
		{
			name: "Extract field with func tag",
			args: args{t: reflect.TypeOf(testC{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  "test",
					typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					useFunc: funcName,
				},
			},
			wantErr: false,
		},
		{
			name: "Extract unexported field with missing func",
			args: args{t: reflect.TypeOf(G{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					typ:     reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					useFunc: funcName,
				},
			},
			wantErr: true,
		},
		{
			name: "Extract unexported field with invalid func",
			args: args{t: reflect.TypeOf(testD{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					typ:     reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					useFunc: funcName,
				},
			},
			wantErr: true,
		},
		{
			name: "Extract field with invalid func argument",
			args: args{t: reflect.TypeOf(testE{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					typ:     reflect.TypeOf((*testInterfaceB)(nil)).Elem(),
					useFunc: funcName,
				},
			},
			wantErr: true,
		},
		{
			name: "Extract field with setter func ptr argument",
			args: args{t: reflect.TypeOf(testF{})},
			want: []diFieldCtx{
				{
					index:   0,
					regKey:  reflect.TypeOf(testA{}),
					typ:     reflect.TypeOf(&testA{}),
					useFunc: funcName,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			registry := diStructCtx{fieldCtxs: make(map[reflect.Type][]diFieldCtx)}
			got, err := registry.getDiInfo(tt.args.t)
			if (err != nil) != tt.wantErr {
				t.Errorf("extractDiInfo() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("extractDiInfo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_extractDiInfo(b *testing.B) {
	b.ReportAllocs()
	type A struct {
		Log  testInterfaceA
		Ta   testA
		Tptr *testB
		num  int
		fn   func() error
	}

	for n := 0; n < b.N; n++ {
		extractDiInfo(reflect.TypeOf(A{}))
	}
}
