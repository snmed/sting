package sting

import (
	"context"
	"fmt"
	"reflect"
	"testing"
)

var testFn = func(ctx context.Context) (interface{}, error) { return &testA{42}, nil }

type testA struct {
	a int
}

type testInterfaceA interface{}

func TestTransientScope(t *testing.T) {
	// Arrange
	scope := TransientScope()

	// Act
	item1, _ := scope.Get(context.Background(), reflect.TypeOf((*testA)(nil)), testFn)
	item2, _ := scope.Get(context.Background(), reflect.TypeOf((*testA)(nil)), testFn)

	// Assert
	if item1 == nil || item2 == nil {
		t.Errorf("item1 expected not nil: %v, item2 expected not nil: %v", item1, item2)
	}

	if item1 == item2 {
		t.Errorf("Expected item1 (%p) is different from item2 (%p)", item1, item2)
	}
}

func TestContainerScope(t *testing.T) {
	// Arrange
	scpCtx := &scopeCtx{items: make(map[interface{}]interface{})}
	ctx := context.WithValue(context.Background(), containerCtxKey, scpCtx)
	scope := ContainerScope()
	cnt := 4

	results := make([]interface{}, cnt)

	// Act
	for i := 0; i < cnt; i++ {
		item, _ := scope.Get(ctx, reflect.TypeOf((*testA)(nil)).Elem(), testFn)
		results[i] = item
	}

	// Assert
	first := results[0]
	if first == nil {
		t.Errorf("No nil items expected: %v", first)
	}
	for i, res := range results[1:] {
		if first != res {
			t.Errorf("Expected all itmes to be the same, first: %v, item %v: %v", first, i, res)
		}
	}

}

type otherScope struct{}

func (o otherScope) Get(c context.Context, i interface{}, a ActionFunc) (interface{}, error) {
	return nil, nil
}

func Test_defaultScopeLifeTimeFunc(t *testing.T) {

	type args struct {
		s Scope
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Transient Scope",
			args{s: TransientScope()},
			10,
		},
		{
			"Container Scope",
			args{s: ContainerScope()},
			10,
		},
		{
			"Request Scope",
			args{s: RequestScope()},
			20,
		},
		{
			"Other Scope",
			args{s: &otherScope{}},
			30,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultScopeLifeTimeFunc(tt.args.s); got != tt.want {
				t.Errorf("defaultScopeLifeTimeFunc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBuilder_Build_with_parent_dependencies(t *testing.T) {

	type I interface{}
	type C struct{}
	type B struct {
		i I
	}
	type A struct {
		b B
	}
	type svcs struct {
		fn  interface{}
		scp Scope
	}
	tests := []struct {
		name             string
		regParent        []svcs
		reg              []svcs
		wantErr          bool
		wantType         reflect.Type
		wantResolveErr   bool
		buildParentFirst bool
	}{
		{
			name: "Fails because parent not built",
			regParent: []svcs{
				{fn: func() (I, error) { return C{}, nil }, scp: TransientScope()},
			},

			reg: []svcs{
				{fn: func(b *B) (A, error) { return A{b: *b}, nil }, scp: TransientScope()},
				{fn: func(i I) (B, error) { return B{i: i}, nil }, scp: TransientScope()},
			},
			wantErr:          true,
			wantType:         reflect.TypeOf(A{}),
			wantResolveErr:   false,
			buildParentFirst: false,
		},
		// TODO: Add more tests when container implementation is done
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parentBuilder := newBuilder()
			builder := parentBuilder.NewBuilder().(*builder)

			for _, svc := range tt.regParent {
				parentBuilder.Register(svc.fn, svc.scp)
			}

			for _, svc := range tt.reg {
				builder.Register(svc.fn, svc.scp)
			}

			var err error
			if tt.buildParentFirst {
				parentBuilder.Build()
				_, err = builder.Build()
			} else {
				_, err = builder.Build()
				parentBuilder.Build()
			}

			if (err != nil) != tt.wantErr {
				t.Fatalf("Builder.Build() error = %v, wantErr false", err)
			}

			if tt.wantErr {
				return
			}

			dep := builder.registrations[tt.wantType]
			item, err := dep.scopeFunc(context.Background())

			if (err != nil) != tt.wantResolveErr {
				t.Fatalf("Builder.Build() error = %v, wantResolveErr false", err)
			}

			// If return ptr change wantType to ptr
			wantType := tt.wantType
			if dep.producePtr {
				wantType = reflect.New(tt.wantType).Type()
			}
			if tp := reflect.TypeOf(item); tp != wantType {
				t.Errorf("Builder.Build() item = %v, wantType %v", tp, tt.wantType)
			}

		})
	}
}

func TestBuilder_Build(t *testing.T) {

	type C struct{}
	type D struct{}
	type I interface{}

	type B struct {
		c C
		d D
	}
	type A struct {
		b B
		c I
	}

	fnA := func(b B, c I) (A, error) {
		return A{b: b, c: c}, nil
	}

	fnB := func(c C, d D) (*B, error) {
		return &B{c: c, d: d}, nil
	}

	fnI := func() (I, error) {
		return C{}, nil
	}

	type fields struct {
		built            bool
		ignoreScopeCheck bool
		lifeTimeFunc     ScopeLifeTimeFunc
	}
	type svcs struct {
		fn  interface{}
		scp Scope
	}
	tests := []struct {
		name           string
		fields         fields
		svc            []svcs
		wantErr        bool
		wantType       reflect.Type
		wantResolveErr bool
	}{
		{
			name: "Resolve struct A",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: TransientScope()},
				{fn: fnA, scp: TransientScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: D{}, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
			},
			wantErr:        false,
			wantType:       reflect.TypeOf(A{}),
			wantResolveErr: false,
		},
		{
			name: "Fails because of register same type twice",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: TransientScope()},
				{fn: fnA, scp: TransientScope()},
				{fn: fnA, scp: TransientScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: D{}, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
			},
			wantErr: true,
		},
		{
			name: "Fails because of cycle in dependency graph",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: TransientScope()},
				{fn: fnA, scp: TransientScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: func(ai testInterfaceA) (D, error) { return D{}, nil }, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
				{fn: func(a A) (testInterfaceA, error) { return C{}, nil }, scp: TransientScope()},
			},
			wantErr:        true,
			wantType:       reflect.TypeOf(D{}),
			wantResolveErr: false,
		},
		{
			name: "Fails because of missing dependency",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: TransientScope()},
				{fn: fnA, scp: TransientScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
			},
			wantErr: true,
		},
		{
			name: "Fails because of higher order scope required",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: RequestScope()},
				{fn: fnA, scp: ContainerScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
				{fn: D{}, scp: ContainerScope()},
			},
			wantErr: true,
		},
		{
			name: "Resolves struct B request scope accepts lower scopes",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: ContainerScope()},
				{fn: fnA, scp: RequestScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
				{fn: D{}, scp: ContainerScope()},
			},
			wantErr:        false,
			wantType:       reflect.TypeOf(B{}),
			wantResolveErr: false,
		},
		{
			name: "Fails because creator function fails",
			fields: fields{
				lifeTimeFunc: DefaultScopeLifeTimeFunc,
			},
			svc: []svcs{
				{fn: fnI, scp: ContainerScope()},
				{fn: fnA, scp: RequestScope()},
				{fn: fnB, scp: TransientScope()},
				{fn: &C{}, scp: TransientScope()},
				{fn: func() (D, error) { return D{}, fmt.Errorf("Failed to create") }, scp: ContainerScope()},
			},
			wantErr:        false,
			wantType:       reflect.TypeOf(B{}),
			wantResolveErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := newBuilder()
			b.built = tt.fields.built
			b.options.ignoreScopeCheck = tt.fields.ignoreScopeCheck
			b.options.lifeTimeFunc = tt.fields.lifeTimeFunc

			for _, svc := range tt.svc {
				b.Register(svc.fn, svc.scp)
			}

			if _, err := b.Build(); (err != nil) != tt.wantErr {
				t.Fatalf("Builder.Build() error = %v, wantErr %v", err, tt.wantErr)
			}

			// If wantErr then go to next test
			if tt.wantErr {
				return
			}

			dep := b.registrations[tt.wantType]
			item, err := dep.scopeFunc(context.Background())

			// If an error occurs while calling scopeFunc
			// check if error wanted and end test.
			if err != nil {
				if (err != nil) != tt.wantResolveErr {
					t.Fatalf("Builder.Build() scopeFunc() = %v, wantResolveErr false", err)
				}
				return
			}

			// If return ptr change wantType to ptr
			wantType := tt.wantType
			if dep.producePtr {
				wantType = reflect.New(tt.wantType).Type()
			}
			if tp := reflect.TypeOf(item); tp != wantType {
				t.Errorf("Builder.Build() scopeFunc = %v, wantType %v", tp, tt.wantType)
			}

		})
	}
}

func Test_evalCreatorFunc(t *testing.T) {

	type args struct {
		node     *depNode
		funcType reflect.Type
	}
	type nodeResult struct {
		wantsType   reflect.Type
		wantsPtr    bool
		wantsParams []reflect.Type
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		result  nodeResult
	}{
		{
			name: "Func without params and interface return",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (testInterfaceA, error) { return nil, nil }),
			},
			wantErr: false,
			result: nodeResult{
				wantsType:   reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Func without params and struct return",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (testA, error) { return testA{}, nil }),
			},
			wantErr: false,
			result: nodeResult{
				wantsType:   reflect.TypeOf(testA{}),
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Func without params and struct ptr return",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (*testA, error) { return &testA{}, nil }),
			},
			wantErr: false,
			result: nodeResult{
				wantsType:   reflect.TypeOf(testA{}),
				wantsPtr:    true,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Invalid function returns error - only error return",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() error { return nil }),
			},
			wantErr: true,
			result: nodeResult{
				wantsType:   nil,
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Invalid function returns error - 3 values returned",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (testInterfaceA, error, int) { return nil, nil, 1 }),
			},
			wantErr: true,
			result: nodeResult{
				wantsType:   nil,
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Invalid function returns error - invalid first type",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (int, error) { return 1, nil }),
			},
			wantErr: true,
			result: nodeResult{
				wantsType:   nil,
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Invalid function returns error - invalid second type",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func() (testInterfaceA, int) { return nil, 2 }),
			},
			wantErr: true,
			result: nodeResult{
				wantsType:   nil,
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
		{
			name: "Func with valid params",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func(a testA, aa *testA, aaa testInterfaceA) (testInterfaceA, error) { return nil, nil }),
			},
			wantErr: false,
			result: nodeResult{
				wantsType: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
				wantsPtr:  false,
				wantsParams: []reflect.Type{
					reflect.TypeOf(testA{}),
					reflect.TypeOf(&testA{}),
					reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
				},
			},
		},
		{
			name: "Func with invalid params",
			args: args{
				node:     &depNode{},
				funcType: reflect.TypeOf(func(a testA, aa *testA, aaa int) (testInterfaceA, error) { return nil, nil }),
			},
			wantErr: true,
			result: nodeResult{
				wantsType:   nil,
				wantsPtr:    false,
				wantsParams: []reflect.Type{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := evalCreatorFunc(tt.args.node, tt.args.funcType); (err != nil) != tt.wantErr {
				t.Errorf("evalCreatorFunc() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.result.wantsType != tt.args.node.produces {
				t.Errorf("depNode.produces = %v, wantsType %v", tt.args.node.produces, tt.result.wantsType)
			}
			if tt.result.wantsPtr != tt.args.node.producePtr {
				t.Errorf("depNode.producePtr = %v, wantsPtr %v", tt.args.node.producePtr, tt.result.wantsPtr)
			}
			if len(tt.result.wantsParams) != len(tt.args.node.parameters) {
				t.Fatalf("depNode.parameters length = %v, wantsParams %v", len(tt.args.node.parameters), len(tt.result.wantsParams))
			}
			for i, v := range tt.result.wantsParams {
				if v != tt.args.node.parameters[i] {
					t.Errorf("depNode.parametes[%v] %v, wantsParams[%v] %v", i, tt.args.node.parameters[i], i, v)
				}
			}
		})
	}
}

func Test_createDepNode(t *testing.T) {
	structTestAPtr := &testA{}
	type args struct {
		key         string
		creatorFunc interface{}
	}
	tests := []struct {
		name     string
		args     args
		wantNode *depNode
		wantErr  bool
	}{
		{
			name: "Create node with struct and key",
			args: args{
				key:         "mystruct",
				creatorFunc: testA{},
			},
			wantNode: &depNode{
				name:         "mystruct",
				produces:     reflect.TypeOf(testA{}),
				creatorFunc:  testA{},
				dependencies: make([]*depNode, 0),
				parameters:   make([]reflect.Type, 0),
			},
			wantErr: false,
		},
		{
			name: "Create node with struct ptr",
			args: args{
				key:         "",
				creatorFunc: structTestAPtr,
			},
			wantNode: &depNode{
				name:         "",
				produces:     reflect.TypeOf(*structTestAPtr),
				producePtr:   true,
				creatorFunc:  structTestAPtr,
				dependencies: make([]*depNode, 0),
				parameters:   make([]reflect.Type, 0),
			},
			wantErr: false,
		},
		{
			name: "Create node with nil creatorFunc returns error",
			args: args{
				key:         "",
				creatorFunc: nil,
			},
			wantNode: nil,
			wantErr:  true,
		},
		{
			name: "Create node with invalid type returns error",
			args: args{
				key:         "",
				creatorFunc: "A string",
			},
			wantNode: nil,
			wantErr:  true,
		},
		{
			name: "Create node with invalid func type returns error",
			args: args{
				key:         "",
				creatorFunc: func(s string) (interface{}, error) { return nil, nil },
			},
			wantNode: nil,
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotNode, err := createDepNode(tt.args.key, tt.args.creatorFunc)
			if (err != nil) != tt.wantErr {
				t.Errorf("createDepNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(gotNode, tt.wantNode) {
				t.Errorf("createDepNode() = %v, want %v", gotNode, tt.wantNode)
			}

		})
	}
}

func TestBuilder_registerService(t *testing.T) {
	type fields struct {
		built            bool
		container        *container
		errors           errors
		ignoreScopeCheck bool
		parent           *builder
		registrations    depMap
		lifeTimeFunc     ScopeLifeTimeFunc
	}
	type args struct {
		key         string
		creatorFunc interface{}
		scope       Scope
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantErr    bool
		wantDepMap depMap
	}{
		{
			name: "Register a function",
			fields: fields{
				built:         false,
				errors:        make([]error, 0),
				registrations: make(depMap, 0),
			},
			args: args{
				creatorFunc: func() (testInterfaceA, error) { return nil, nil },
				scope:       TransientScope(),
			},
			wantErr: false,
			wantDepMap: depMap{
				reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{
					produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					scope:    TransientScope(),
				},
			},
		},
		{
			name: "Register already registered func",
			fields: fields{
				built:  false,
				errors: make([]error, 0),
				registrations: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{
						produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
						scope:    TransientScope(),
					},
				},
			},
			args: args{
				creatorFunc: func() (testInterfaceA, error) { return nil, nil },
				scope:       TransientScope(),
			},
			wantErr: true,
			wantDepMap: depMap{
				reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{
					produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					scope:    TransientScope(),
				},
			},
		},
		{
			name: "Register already registered func with name",
			fields: fields{
				built:  false,
				errors: make([]error, 0),
				registrations: depMap{
					reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{
						produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
						scope:    TransientScope(),
					},
				},
			},
			args: args{
				key:         "mysvc",
				creatorFunc: func() (testInterfaceA, error) { return nil, nil },
				scope:       TransientScope(),
			},
			wantErr: false,
			wantDepMap: depMap{
				reflect.TypeOf((*testInterfaceA)(nil)).Elem(): &depNode{
					produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					scope:    TransientScope(),
				},
				namedSvcKey("mysvc"): &depNode{
					name:     "mysvc",
					produces: reflect.TypeOf((*testInterfaceA)(nil)).Elem(),
					scope:    TransientScope(),
				},
			},
		},
		{
			name: "Register service on built Builder",
			fields: fields{
				built:         true,
				errors:        make([]error, 0),
				registrations: depMap{},
			},
			args: args{
				creatorFunc: func() (testInterfaceA, error) { return nil, nil },
				scope:       TransientScope(),
			},
			wantErr:    true,
			wantDepMap: depMap{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &builder{
				built:     tt.fields.built,
				container: tt.fields.container,
				errors:    tt.fields.errors,
				options: Options{
					ignoreScopeCheck: tt.fields.ignoreScopeCheck,
					lifeTimeFunc:     tt.fields.lifeTimeFunc,
				},
				parent:        tt.fields.parent,
				registrations: tt.fields.registrations,
			}
			b.registerService(tt.args.key, tt.args.creatorFunc, tt.args.scope)

			if (len(b.errors) != 0) != tt.wantErr {
				t.Errorf("registerService() errors = %v, want %v", (len(b.errors) != 0), tt.wantErr)
			}

			if len(b.registrations) != len(tt.wantDepMap) {
				t.Errorf("registerService() registrations length = %v, want %v", len(b.registrations), len(tt.wantDepMap))
			}

			for k, wants := range tt.wantDepMap {
				val, ok := b.registrations[k]
				if !ok {
					t.Errorf("depMap key = %v, not found", k)
					continue
				}

				if val.name != wants.name {
					t.Errorf("depMap key = %v, value = %v, want %v", k, val.name, wants.name)
				}

				if val.producePtr != wants.producePtr {
					t.Errorf("depMap key = %v, value = %v, want %v", k, val.producePtr, wants.producePtr)
				}

				if val.produces != wants.produces {
					t.Errorf("depMap key = %v, value = %v, want %v", k, val.produces, wants.produces)
				}

				if val.scope != wants.scope {
					t.Errorf("depMap key = %v, value = %v, want %v", k, val.scope, wants.scope)
				}

				if len(val.parameters) != len(wants.parameters) {
					t.Errorf("parameters key = %v, value = %v, want %v", k, len(val.parameters), len(wants.parameters))
					continue
				}

				for idx, p := range val.parameters {
					if p != wants.parameters[idx] {
						t.Errorf("parameters key = %v, value = %v, want %v", k, p, wants.parameters[idx])
					}
				}

			}

		})
	}
}

func Test_hasCycle(t *testing.T) {
	type args struct {
		n              *depNode
		processedNodes []*depNode
	}

	node1 := &depNode{
		name:     "A",
		produces: reflect.TypeOf(testA{}),
	}
	node2 := &depNode{
		name:     "B",
		produces: reflect.TypeOf(testA{}),
		dependencies: []*depNode{
			node1,
		},
	}
	node3 := &depNode{
		name:     "D",
		produces: reflect.TypeOf(testA{}),
		dependencies: []*depNode{
			node2,
		},
	}
	node1.dependencies = []*depNode{
		node3,
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "No cycles exists",
			args: args{
				n: &depNode{
					dependencies: []*depNode{
						{},
						{
							dependencies: []*depNode{
								{},
								{},
							},
						},
					},
				},
				processedNodes: []*depNode{},
			},
			wantErr: false,
		},
		{
			name: "Cycles exists",
			args: args{
				n: &depNode{
					name:     "C",
					produces: reflect.TypeOf(testA{}),
					dependencies: []*depNode{
						node1,
						{
							dependencies: []*depNode{
								{},
								{},
							},
						},
					},
				},
				processedNodes: []*depNode{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := checkCycle(tt.args.n, tt.args.processedNodes)
			if (err != nil) != tt.wantErr {
				t.Errorf("hasCycle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_contextScope_Get(t *testing.T) {
	type A struct {
		txt string
	}
	a := &A{"A Test"}
	type fields struct {
		key stingCtxKey
	}
	type args struct {
		ctx    context.Context
		t      reflect.Type
		action ActionFunc
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:   "Stores and return",
			fields: fields{key: stingCtxKey("test")},
			args: args{
				ctx: context.WithValue(context.Background(), stingCtxKey("test"),
					&scopeCtx{items: make(map[interface{}]interface{}, 0)}),
				t: reflect.TypeOf((*A)(nil)),
				action: func(ctx context.Context) (interface{}, error) {
					return a, nil
				},
			},
			want:    a,
			wantErr: false,
		},
		{
			name:   "Stores and fails",
			fields: fields{key: stingCtxKey("test")},
			args: args{
				ctx: context.WithValue(context.Background(), stingCtxKey("test"),
					&scopeCtx{items: make(map[interface{}]interface{}, 0)}),
				t: reflect.TypeOf((*A)(nil)),
				action: func(ctx context.Context) (interface{}, error) {
					return nil, fmt.Errorf("An error")
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := contextScope{
				key: tt.fields.key,
			}
			got, err := s.Get(tt.args.ctx, tt.args.t, tt.args.action)
			if (err != nil) != tt.wantErr {
				t.Errorf("contextScope.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("contextScope.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
