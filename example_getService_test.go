package sting_test

import "log"
import "fmt"
import "bitbucket.org/snmed/sting"

// This example shows how to register multiple services with the
// same type and how to retrieve services from the di container.
func Example_getService() {

	container, err := sting.NewBuilder().
		// Register a pointer to struct with a  container scope
		Register(&Config{DefaultId: 42, DefaultName: "Judge Dredd"}, sting.ContainerScope()).
		// Register a creator function with a dependency and container scope
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: c.DefaultId, name: c.DefaultName}, nil
			},
			sting.RequestScope(),
		).
		// Register a named service with an already registered type
		RegisterNamed(
			"myconfig",
			func() (*Config, error) { return &Config{DefaultId: 11, DefaultName: "Selene"}, nil },
			sting.RequestScope(),
		).
		Build()

	if err != nil {
		log.Fatalln(err)
	}

	//Get a service by passing a nil pointer to an interface
	svc, _, err := container.GetService((*PersonRepository)(nil))
	if err != nil {
		log.Fatalln(err)
	}

	// Get a service by passing a nil pointer to a struct
	cfg, _, err := container.GetService((*Config)(nil))
	if err != nil {
		log.Fatalln(err)
	}

	// Get a named service
	mycfg, ctx, err := container.GetService("myconfig")
	if err != nil {
		log.Fatalln(err)
	}

	// Change values
	mycfg.(*Config).DefaultName = "Tom Bombadil"
	mycfg.(*Config).DefaultId = 9999

	// Get the same named service by passing the context
	cfg2, _, err := container.GetService("myconfig", ctx)
	if err != nil {
		log.Fatalln(err)
	}

	// Get the named service without a context, same as a new request.
	cfg3, _, err := container.GetService("myconfig")
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(svc.(PersonRepository).Find(42))
	fmt.Println(cfg)
	fmt.Println(mycfg)
	fmt.Println(cfg2)
	fmt.Println(cfg3)
	// Output: &{42 Judge Dredd}
	// &{Judge Dredd 42}
	// &{Tom Bombadil 9999}
	// &{Tom Bombadil 9999}
	// &{Selene 11}
}
