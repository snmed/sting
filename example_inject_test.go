package sting_test

import (
	"fmt"
	"log"

	"bitbucket.org/snmed/sting"
)

// This example shows how to inject dependencies into
// structs and functions.
func Example_inject() {
	container, err := sting.NewBuilder().
		// Register a pointer to struct with a  container scope
		Register(Config{DefaultId: 42, DefaultName: "Judge Dredd"}, sting.ContainerScope()).
		// Register a creator function with a dependency and container scope
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: c.DefaultId, name: c.DefaultName}, nil
			},
			sting.RequestScope(),
		).
		// Register a named service with an already registered type
		RegisterNamed(
			"myconfig",
			func() (*Config, error) { return &Config{DefaultId: 11, DefaultName: "Selene"}, nil },
			sting.RequestScope(),
		).
		Build()

	controller := &Controller{}

	// Inject dependencies into a struct
	_, err = container.Inject(controller)
	if err != nil {
		log.Fatalln(err)
	}

	var config *Config
	var repo PersonRepository

	// Inject dependencies into a function
	container.Inject(func(c *Config, r PersonRepository) {
		config = c
		config.DefaultId += c.DefaultId
		config.DefaultName += " changed"
		repo = r
	})

	fmt.Println(controller.config)
	fmt.Println(controller.MyConfig)
	fmt.Println(controller.ignored)
	fmt.Println(controller.repo)
	fmt.Println(controller.Access)
	fmt.Println(config)
	fmt.Println(repo)
	// Output: &{Judge Dredd 42}
	// {Selene 11}
	// <nil>
	// {Judge Dredd 42}
	// {Selene 11}
	// &{Judge Dredd changed 84}
	// {Judge Dredd 42}

}
