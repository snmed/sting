package sting

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"reflect"
	"sync"
	"time"
)

// Used to create a unique container id.
var containerRandom = rand.New(rand.NewSource(time.Now().UnixNano()))

// Creates a unique containerKey. Should be unique enough for a small
// numbers of containers.
func createContainerID() ContainerID {
	return ContainerID(fmt.Sprintf("%x", containerRandom.Int63()))
}

// ActionFunc executes initialisation work and returns an instance
// of a dependency object.
type ActionFunc func(ctx context.Context) (interface{}, error)

// InjectionHandler is a HandlerFunc with an arbitrary amount of additional parameters.
// All additional parameters must be of type struct, pointer to struct or interface.
// InjectionHandler must be a function in form of:
//  func(http.ResponseWriter, *http.Request, [additional parameters...] )
type InjectionHandler interface{}

// ContainerID is an unique container identifier which differs for every container.
type ContainerID string

// Container is responsible to resolve, construct and inject dependencies.
type Container interface {
	// ID returns the unique identifier for this Container.
	ID() ContainerID
	// Injects dependencies into a struct or function, accepts pointer to struct and functions.
	// Request scope needs a context as second argument, otherwise it works like a transient scope.
	// Only the first context arguemnt is used, all additional passed contexts will be ignored.
	Inject(interface{}, ...context.Context) (context.Context, error)
	// Gets a service, accepts only pointer to interface with a nil value ex. (*io.Closer)(nil),
	// pointer to struct or a string for a named service. It returns the interface or a pointer to a struct.
	// Request scope needs a context as second argument, otherwise it works like a transient scope.
	// Only the first context arguemnt is used, all additional passed contexts will be ignored.
	GetService(interface{}, ...context.Context) (interface{}, context.Context, error)
	// Injects all dependencies into a InjectionHandler. Returns a http.Handler.
	// Panics if any dependency is missing in the container.
	// Request scope requires the use of the UseHTTPRequestContext middleware.
	HandlerFunc(InjectionHandler) http.Handler
}

// Ensure all interface method are implemented
var _ Container = &container{}

type container struct {
	m            sync.Mutex
	id           ContainerID
	parent       *container
	registry     depMap
	containerCtx *scopeCtx
}

func (c *container) ID() ContainerID {
	return c.id
}

// Inject injects dependencies into a struct or function, accepts pointer to struct and functions.
func (c *container) Inject(target interface{}, ctxs ...context.Context) (context.Context, error) {
	v := reflect.ValueOf(target)
	t := v.Type()
	if !(t.Kind() == reflect.Func ||
		(t.Kind() == reflect.Ptr && t.Elem().Kind() == reflect.Struct)) {
		return nil, serr{
			err: errInvalidInjectionTarget,
			msg: fmt.Sprintf("Type %v is a invalid target, only func or ptr to struct accepted", t),
		}
	}

	c.m.Lock()
	defer c.m.Unlock()

	ctx := ensureReqCtx(reduceCtx(ctxs), c.id)
	if t.Kind() == reflect.Func {
		return ctx, c.injectIntoFunc(c.wrapCtx(ctx), v)
	}

	return ctx, c.injectIntoStruct(c.wrapCtx(ctx), v)
}

// injectInfoFunc calls a function value with all required dependencies. It
// returns an error if parameter types are invalid or a dependency is missing.
func (c *container) injectIntoFunc(ctx context.Context, v reflect.Value) error {
	t := v.Type()
	args := make([]reflect.Value, 0, t.NumIn())

	for i := 0; i < t.NumIn(); i++ {
		argT := t.In(i)
		if !isSupportedDiType(argT) {
			return serr{
				err: errInvalidInjectionTarget,
				msg: fmt.Sprintf("All arguments of a target function must be struct, ptr to struct or interface"),
			}
		}

		n, ok := c.getNode(argT, argT)
		if !ok {
			return serr{
				err: errMissingDependency,
				msg: fmt.Sprintf("Missing dependency '%v' needs '%v'", t, argT),
			}
		}

		svc, err := n.scope.Get(ctx, n.getScopeKey(), n.scopeFunc)
		if err != nil {
			return err
		}
		args = append(args, getInputParam(argT, svc))
	}

	v.Call(args)
	return nil
}

// injectIntoStruct is responsible to inject dependencies into a struct. It takes a reflected value
// of a pointer to struct and a context for dependency resolution. If target is not a pointer to struct,
// this function panics.
func (c *container) injectIntoStruct(ctx context.Context, target reflect.Value) error {
	fc, err := diStructCtxRegistry.getDiInfo(target.Type())
	if err != nil {
		return err
	}

	for _, f := range fc {
		if err := c.setDependency(ctx, target, target.Elem().Field(f.index), f); err != nil {
			return err
		}
	}

	return nil
}

// setDependency calls the setter function or sets the dependency directly on the value.
func (c *container) setDependency(ctx context.Context, target, v reflect.Value, fctx diFieldCtx) error {

	// Is an embedded field so call recursive
	if len(fctx.embeddedFields) > 0 {
		for _, fc := range fctx.embeddedFields {
			if err := c.setDependency(ctx, target, v.Field(fc.index), fc); err != nil {
				return err
			}
		}
		return nil
	}
	// We have a setter
	if fctx.useFunc != "" {
		return c.callSetter(ctx, target, fctx)
	}

	if !v.CanSet() {
		// We shouldn't get here, if so then there is a bug in this package
		return serr{
			err: errUnexpectedError,
			msg: fmt.Sprintf("Mismatched type information for %v get %v", v, fctx),
		}
	}

	arg, err := c.getSvcValue(ctx, fctx)
	if err != nil {
		return err
	}

	v.Set(arg)
	return nil
}

func (c *container) callSetter(ctx context.Context, v reflect.Value, fctx diFieldCtx) error {
	fn := v.MethodByName(fctx.useFunc)
	if !fn.IsValid() {
		// We should never get here, if so then it's a bug
		return serr{
			err: errInvalidInjectionTarget,
			msg: fmt.Sprintf("Setter function %v not found", fctx.useFunc),
		}
	}

	arg, err := c.getSvcValue(ctx, fctx)
	if err != nil {
		return err
	}

	fn.Call([]reflect.Value{arg})
	return nil
}

// getSvcValue gets the required depNode from the container registry
// and calls the scope methode 'Get'. It packs the service into
// a reflect.Value and returns it.
func (c *container) getSvcValue(ctx context.Context, fctx diFieldCtx) (value reflect.Value, err error) {
	n, ok := c.getNode(fctx.regKey, fctx.typ)
	if !ok && c.parent != nil {
		var svc interface{}
		switch k := fctx.regKey.(type) {
		case string:
			svc, _, err = c.parent.GetService(k, ctx)
		case reflect.Type:
			svc, _, err = c.parent.GetService(ensureGetServiceArg(k), ctx)
		default:
			svc, _, err = c.parent.GetService(ensureGetServiceArg(reflect.TypeOf(fctx.regKey)), ctx)
		}
		if err != nil {
			return value, err
		}
		return getInputParam(fctx.typ, svc), err
	}

	if !ok {
		return value, serr{
			err: errMissingDependency,
			msg: fmt.Sprintf("Dependency %v with key %v not found", fctx.typ, fctx.regKey),
		}
	}

	svc, err := n.scope.Get(ctx, n.getScopeKey(), n.scopeFunc)
	if err != nil {
		return value, err
	}

	return getInputParam(fctx.typ, svc), nil
}

func reduceCtx(ctxs []context.Context) context.Context {
	if len(ctxs) > 0 {
		return ctxs[0]
	}
	return nil
}

// GetService gets a service, accepts only pointer to interface with a nil value ex. (*io.Closer)(nil),
// pointer to struct or a string for a named service.
func (c *container) GetService(request interface{}, ctxs ...context.Context) (svc interface{}, ctx context.Context, err error) {
	typ := reflect.TypeOf(request)
	if !(typ.Kind() == reflect.String ||
		(typ.Kind() == reflect.Ptr &&
			(typ.Elem().Kind() == reflect.Interface || typ.Elem().Kind() == reflect.Struct))) {
		return nil, nil, serr{
			err: errInvalidServiceRequest,
			msg: fmt.Sprintf("Requested service is %v not pointer to interface, pointer to struct or a string", typ),
		}
	}

	c.m.Lock()
	defer c.m.Unlock()

	ctx = ensureReqCtx(reduceCtx(ctxs), c.id)

	n, ok := c.getNode(request, typ)
	if !ok {
		if c.parent != nil {
			// Try to get service form the parent container
			svc, ctx, err = c.parent.GetService(request, ctx)
			return
		}

		err := serr{
			err: errServiceNotFound,
			msg: fmt.Sprintf("Service %v not found", request),
		}
		if request == nil {
			err.msg = fmt.Sprintf("Service %v not found", typ)
		}

		return nil, nil, err
	}

	svc, err = n.scope.Get(c.wrapCtx(ctx), n.getScopeKey(), n.scopeFunc)
	if !n.producePtr && n.produces.Kind() != reflect.Interface {
		val := reflect.New(n.produces)
		val.Elem().Set(reflect.ValueOf(svc))
		svc = val.Interface()
	}
	return svc, ctx, err
}

// HandlerFunc injects all dependencies into a InjectionHandler. Returns a http.Handler.
// Panics if any dependency is missing in the container.
func (c *container) HandlerFunc(handler InjectionHandler) http.Handler {
	t, vhandler := reflect.TypeOf(handler), reflect.ValueOf(handler)
	if !isInjectionHandler(t) {
		panic("Passed parameter is not a InjectionHandler")
	}

	nodes, params := []interface{}{}, []reflect.Type{}
	for i := 2; i < t.NumIn(); i++ {
		p := t.In(i)
		params = append(params, p)
		if !isSupportedDiType(p) {
			panic("Unsupported types in passed InjectionHandler")
		}
		if p.Kind() == reflect.Ptr {
			p = p.Elem()
		}

		if node, ok := c.registry[p]; ok {
			nodes = append(nodes, node)
			continue
		}
		if c.parent != nil {
			if _, ok := c.parent.registry[p]; ok {
				nodes = append(nodes, p)
				continue
			}
		}

		panic(fmt.Sprintf("Missing dependency %v required in passed InjectionHandler", p))
	}

	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		c.m.Lock()
		reqCtx := req.Context()
		ctx := c.wrapCtx(ensureReqCtx(reqCtx, c.id))

		// ensureReqCtx returns the same context if a key already exists.
		if reqCtx != ctx {
			req = req.WithContext(ctx)
		}

		values := make([]reflect.Value, t.NumIn())
		values[0], values[1] = reflect.ValueOf(w), reflect.ValueOf(req)

		cnt := 2
		for i := range nodes {
			n := nodes[i]
			var err error
			var val interface{}

			switch t := n.(type) {
			case *depNode:
				val, err = t.scope.Get(ctx, t.getScopeKey(), t.scopeFunc)
			case reflect.Type:
				val, _, err = c.parent.GetService(ensureGetServiceArg(t), ctx)
			default:
				panic("Library error, contact developer") // We should never get here, but in case fail fast.
			}
			if err != nil {
				c.m.Unlock()
				panic(err)
			} else if val == nil {
				c.m.Unlock()
				panic("ScopeFunc returned a nil value")
			}
			values[cnt] = getInputParam(params[i], val)
			cnt++
		}
		c.m.Unlock()
		vhandler.Call(values)
	})
}

// getNode does a lookup in the registry for a requested service and
// returns a valid depNode pointer or nil if not found
func (c *container) getNode(i interface{}, typ reflect.Type) (node *depNode, ok bool) {
	// If i is a string, then we lookup a named service.
	s, ok := i.(string)
	if ok {
		node, ok = c.registry[namedSvcKey(s)]
		return
	}

	// If it's a struct then we directly lookup dependency in the registry.
	if typ.Kind() == reflect.Struct {
		node, ok = c.registry[typ]
		return
	}

	// Last but not least, check if it's an interface, pointer to interface or a pointer to struct.
	if k := typ.Kind(); k == reflect.Ptr &&
		(typ.Elem().Kind() == reflect.Struct || typ.Elem().Kind() == reflect.Interface) {
		node, ok = c.registry[typ.Elem()]
		return
	} else if k == reflect.Interface {
		node, ok = c.registry[typ]
		return
	}

	return nil, false
}

// wrapCtx wraps container and request context into a context
// so the Get function of Scope gets the correct scopeCtx.
func (c *container) wrapCtx(ctx context.Context) context.Context {
	ctx = context.WithValue(ctx, containerCtxKey, c.containerCtx)
	return context.WithValue(ctx, requestCtxKey, ctx.Value(c.id))
}

// ensureReqCtx verifies if request scopeCtx is available in ctx, if not it creates
// a scopeCtx and returns it. Returns always a non nil context.Context.
func ensureReqCtx(ctx context.Context, id ContainerID) context.Context {
	if ctx == nil {
		ctx = context.Background()
	}

	rctx := ctx.Value(id)
	if rctx == nil {
		return context.WithValue(ctx, id, &scopeCtx{items: make(map[interface{}]interface{}, 0)})
	} else if _, ok := rctx.(*scopeCtx); !ok {
		return context.WithValue(ctx, id, &scopeCtx{items: make(map[interface{}]interface{}, 0)})
	}
	return ctx
}

var respWriterType = reflect.TypeOf((*http.ResponseWriter)(nil)).Elem()
var requestType = reflect.TypeOf((*http.Request)(nil))

// isInjectionHandler verifies if type is a valid InjectionHandler.
func isInjectionHandler(t reflect.Type) bool {
	if t.Kind() != reflect.Func ||
		t.NumIn() < 2 ||
		t.In(0) != respWriterType ||
		t.In(1) != requestType {
		return false
	}
	return true
}

func newContainer() *container {
	return &container{
		id:           createContainerID(),
		registry:     make(depMap, 0),
		containerCtx: &scopeCtx{items: make(map[interface{}]interface{}, 0)},
	}
}
