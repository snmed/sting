package sting

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"sync"
)

// VERSION is the current sting version
const VERSION = "1.0.0"

// NewBuilder returns a preconfigured Builder ready to use.
func NewBuilder(c ...Configure) Builder {
	return newBuilder(c...)
}

// UseScopeLifeTimeFunc configures the Builder to use the supplied
// function to check life time violations. This is useful if a custom
// scope is used and overwrites the default life time function.
func UseScopeLifeTimeFunc(fn ScopeLifeTimeFunc) Configure {
	return func(o *Options) {
		o.lifeTimeFunc = fn
	}
}

// UseLifeTimeCheck enables or disables lifetime checks during container build process.
func UseLifeTimeCheck(b bool) Configure {
	return func(o *Options) {
		o.ignoreScopeCheck = !b
	}
}

// isSupportedDiType verifies if the type t is a valid parameter for a creatorFunc
func isSupportedDiType(t reflect.Type) bool {
	return t != nil && (t.Kind() == reflect.Struct ||
		t.Kind() == reflect.Interface ||
		(t.Kind() == reflect.Ptr && t.Elem().Kind() == reflect.Struct))
}

type depMap map[interface{}]*depNode

func (dm depMap) clone() depMap {
	newMap := make(depMap, len(dm))
	for k, v := range dm {
		newMap[k] = v
	}
	return newMap
}

// depNode holds informations about a registered service as
// required dependencies, scope and functions that creates the service.
type depNode struct {
	name         string
	produces     reflect.Type
	producePtr   bool
	parameters   []reflect.Type
	scope        Scope
	dependencies []*depNode
	creatorFunc  interface{}
	scopeFunc    ActionFunc
}

func (n *depNode) getScopeKey() interface{} {
	if n.name != "" {
		return n.name
	}
	return n.produces
}

const (
	diIgnore = "ignore"
	diFunc   = "func"
	diName   = "name"
)

var diStructCtxRegistry = diStructCtx{fieldCtxs: make(map[reflect.Type][]diFieldCtx)}

// Holds information about structs and their dependencies
type diStructCtx struct {
	mu        sync.RWMutex
	fieldCtxs map[reflect.Type][]diFieldCtx
}

// getDiInfo returns all those fields who needs a dependency.
func (d *diStructCtx) getDiInfo(t reflect.Type) ([]diFieldCtx, error) {
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	d.mu.RLock()
	if v, ok := d.fieldCtxs[t]; ok {
		d.mu.RUnlock()
		return v, nil
	}
	d.mu.RUnlock()

	return d.register(t)
}

func (d *diStructCtx) register(t reflect.Type) ([]diFieldCtx, error) {
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	d.mu.Lock()
	defer d.mu.Unlock()
	if v, ok := d.fieldCtxs[t]; ok {
		return v, nil
	}

	ctxs, err := extractDiInfo(t)
	if err != nil {
		return nil, err
	}
	d.fieldCtxs[t] = ctxs
	return ctxs, nil
}

// extractDiInfo collects information about fields and functions of a struct.
// It excpects a type of struct and returns information of all fields that
// needs a dependency.
func extractDiInfo(t reflect.Type) ([]diFieldCtx, error) {
	// Less allocations
	infos := make([]diFieldCtx, t.NumField())[:0]
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		cfg := field.Tag.Get("di")
		fnOk, fn := tagValue(diFunc, cfg)
		// Skip ignored fiekds
		if ok, _ := tagValue(diIgnore, cfg); ok {
			continue
		}

		// Embbeded types must be called recursive
		if field.Anonymous {
			ctxs, err := extractDiInfo(field.Type)
			if err != nil {
				return nil, err
			}
			infos = append(infos, diFieldCtx{index: i, typ: nil, regKey: nil, embeddedFields: ctxs})
			continue
		}

		// Skip any not supported types, fields with unsupported types
		// but with func and name set, can be used.
		if !isSupportedDiType(field.Type) && fn == "" {
			continue
		}

		di := diFieldCtx{index: i, typ: field.Type, regKey: field.Type}
		// If its a pointer, regKey is the type which the pointer points to
		if field.Type.Kind() == reflect.Ptr {
			di.regKey = field.Type.Elem()
		}

		// Get setter function if func is set or if field is unexported
		if fnOk || field.PkgPath != "" {
			if !fnOk {
				fn = fmt.Sprintf("Set%v", strings.Title(field.Name))
			}

			err := extractDiSetter(t, fn, &di)
			if err != nil {
				return nil, err
			}
		}

		// If name tag is set, change regKey to a namedSvcKey
		if ok, name := tagValue(diName, cfg); ok {
			di.regKey = name
		}
		infos = append(infos, di)
	}

	return infos, nil
}

// extractDiSetter sets the index and argument type of the setter function, if the setter is invalid
// an error is returned.
func extractDiSetter(t reflect.Type, name string, di *diFieldCtx) error {
	m, ok := reflect.New(t).Type().MethodByName(name)
	if !ok {
		return serr{
			err: errInvalidInjectionTarget,
			msg: fmt.Sprintf("Function %v not found for type %v", name, t),
		}
	}

	if m.Type.NumIn() != 2 {
		return serr{
			err: errInvalidInjectionTarget,
			msg: fmt.Sprintf("Setter function %v requires exact 1 argument get %v", name, m.Type.NumIn()-1),
		}
	}

	mt := m.Type.In(1)
	if !isSupportedDiType(mt) {
		return serr{
			err: errInvalidInjectionTarget,
			msg: fmt.Sprintf("Setter function has wrong argument type %v; must be interface, struct or ptr to struct", mt),
		}
	}

	di.useFunc, di.regKey, di.typ = m.Name, mt, mt
	if mt.Kind() == reflect.Ptr {
		di.regKey = mt.Elem()
	}

	return nil
}

func tagValue(key, s string) (bool, string) {
	for _, flag := range strings.Split(s, ",") {
		parts := strings.Split(flag, "=")
		if parts[0] != key {
			continue
		}

		if len(parts) < 2 {
			return true, ""
		}
		return true, parts[1]
	}

	return false, ""
}

// diFieldCtx contains relevant di field information of a struct
// in which dependencies will be injected.
type diFieldCtx struct {
	index          int          // Index of the field
	regKey         interface{}  // Key in the container registry
	useFunc        string       // Use a function to set value
	typ            reflect.Type // Type of the field
	embeddedFields []diFieldCtx // If embedded type process this fields to
}

func formatDepMap(mm []*depNode, lastNode *depNode) string {
	var buf bytes.Buffer
	delimiter := " -> "
	for _, k := range mm {
		buf.WriteString(fmt.Sprintf("%v", k.produces))
		buf.WriteString(delimiter)
	}
	buf.WriteString(fmt.Sprintf("%v", lastNode.produces))

	return buf.String()
}
