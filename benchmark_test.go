package sting_test

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"bitbucket.org/snmed/sting"
)

type (
	benchA struct {
		Num   int    `di:"func=SetNum"`
		msg   string `di:"func=SetMsg"`
		Bench benchI
		BB    benchB `di:"name=bench"`
	}

	benchI interface {
		Say() string
	}

	benchB struct {
		msg string
	}

	benchC struct {
		num int
	}
)

func (a *benchA) SetNum(c benchC) {
	a.Num = c.num
}

func (a *benchA) SetMsg(b benchB) {
	a.msg = b.msg
}

func (b benchB) Say() string {
	return b.msg
}

func BenchHandlerFunc(w http.ResponseWriter, r *http.Request, i benchI, c *benchC) {
	w.Write([]byte(i.Say()))
	w.Write([]byte(strconv.Itoa(c.num)))
}

func Benchmark_HandlerFunc(b *testing.B) {
	b.ReportAllocs()
	builder := sting.NewBuilder()
	container, err := builder.Register(benchB{"Hello World"}, sting.TransientScope()).
		Register(benchC{42}, sting.ContainerScope()).
		Register(func() (benchI, error) { return benchB{"What else"}, nil }, sting.RequestScope()).
		RegisterNamed("bench", func(b benchB) (*benchB, error) { return &benchB{b.msg + "!!"}, nil }, sting.TransientScope()).
		Build()
	if err != nil {
		b.Fatal(err)
	}
	fn := container.HandlerFunc(BenchHandlerFunc)
	for i := 0; i < b.N; i++ {
		fn.ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("GET", "/", nil))
	}
}

func Benchmark_HandlerFunc_Direct(b *testing.B) {
	b.ReportAllocs()
	var i benchI = benchB{"What Else!!"}
	c := benchC{42}
	fn := func(w http.ResponseWriter, r *http.Request) {
		BenchHandlerFunc(w, r, i, &c)
	}
	for i := 0; i < b.N; i++ {
		fn(httptest.NewRecorder(), httptest.NewRequest("GET", "/", nil))
	}
}
