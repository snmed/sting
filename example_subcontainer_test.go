package sting_test

import (
	"fmt"
	"log"
	"net/http/httptest"

	"bitbucket.org/snmed/sting"
)

// This example shows how to use the di container in a web application or a microservice.
// If request scope is used, wrap first handler of a handler chain within the UseHTTPRequestContext middleware.
// Afterwards wrap any InjectionHandler inside a container.HandlerFunc.
func Example_subContainer() {

	// Create the first builder and register services
	builder := sting.NewBuilder().
		Register(&Config{DefaultId: 42, DefaultName: "Judge Dredd"}, sting.TransientScope()).
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: c.DefaultId, name: c.DefaultName}, nil
			},
			sting.RequestScope(),
		).
		Register(
			func() (*Person, error) { return &Person{Id: 11, Name: "Selene"}, nil },
			sting.RequestScope(),
		)

	// Create a child builder and register services
	childBuilder := builder.NewBuilder().
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: 2 * c.DefaultId, name: "Child " + c.DefaultName}, nil
			},
			sting.ContainerScope(),
		)

	// Important build parent builder first
	container, perr := builder.Build()
	subcontainer, serr := childBuilder.Build()

	if perr != nil || serr != nil {
		log.Fatalln(perr, serr)
	}

	// Now use containers in handler chains
	parentHandler := LogMiddleware(container.HandlerFunc(FindPerson))
	subHandler := LogMiddleware(subcontainer.HandlerFunc(FindPerson))

	precorder := httptest.NewRecorder()
	parentHandler.ServeHTTP(precorder, httptest.NewRequest("GET", "/api?id=55", nil))

	fmt.Println(precorder.Body)

	precorder = httptest.NewRecorder()
	parentHandler.ServeHTTP(precorder, httptest.NewRequest("GET", "/api", nil))

	fmt.Println(precorder.Body)

	srecorder := httptest.NewRecorder()
	subHandler.ServeHTTP(srecorder, httptest.NewRequest("GET", "/api?id=55", nil))

	fmt.Println(srecorder.Body)

	srecorder = httptest.NewRecorder()
	subHandler.ServeHTTP(srecorder, httptest.NewRequest("GET", "/api", nil))

	fmt.Println(srecorder.Body)
	// Output:
	// Calling URL /api?id=55 with method GET ...
	// &{55 Judge Dredd}
	// Calling URL /api with method GET ...
	// {11 Selene}
	// Calling URL /api?id=55 with method GET ...
	// &{55 Child Judge Dredd}
	// Calling URL /api with method GET ...
	// {11 Selene}

}
