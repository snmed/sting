//go:generate generr -file $GOFILE

package sting

import "bytes"

type errorT int

// @generr
const (
	errInvalidCreatorFunc       errorT = iota // IsInvalidCreatorFunc checks if an error is caused by an invalid creator function.
	errServiceAlreadyRegistered               // IsServiceAlreadyRegistered checks if an error is caused by registering a service twice.
	errContainerAlreadyBuilt                  // IsContainerAlreadyBuilt checks if an error is caused by registering a service on an already built container.
	errParentContainerNotBuilt                // IsParentContainerNotBuilt checks if an error is caused by calling Build on sub container with a not build parent container.
	errCycleDetected                          // IsCycleDetected checks if an error is caused by a cycle in the dependency graph.
	errLifetimeViolation                      // IsLifetimeViolation checks if an error is caused by a lifetime violation.
	errMissingDependency                      // IsMissingDependency checks if an error is caused by a missing dependency.
	errActionFuncReturnedNil                  // IsActionFuncReturnedNil checks if an error is caused by an action function which returns nil.
	errInvalidInjectionHandler                // IsInvalidInjectionHandler checks if an error is caused by an invalid injection handler.
	errServiceNotFound                        // IsServiceNotFound checks if an error is caused by a unresolved dependency.
	errInvalidServiceRequest                  // IsInvalidServiceRequest checks if an error is caused by a call to GetService with an invalid argument.
	errInvalidInjectionTarget                 // IsInvalidInjectionTarget checks if an error is caused by a call to Inject with an invalid target.
	errUnexpectedError                        // IsUnexpectedError checks if an error is an unexpected error.
)

// serr is an internal used error type.
type serr struct {
	err errorT
	msg string
}

func (e serr) Error() string {
	return e.msg
}

type errors []error

func (e errors) Error() string {
	var buf bytes.Buffer
	for _, item := range e {
		buf.WriteString(item.Error() + ": ")
	}
	return buf.String()
}

func (e errors) errorOrNil() error {
	if len(e) == 0 {
		return nil
	}
	return e
}
