package sting_test

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/snmed/sting"
)

type (
	PersonRepository interface {
		Find(int) *Person
		FindByName(string) *Person
	}

	PersonAccess struct {
		name string
		id   int
	}

	Config struct {
		DefaultName string
		DefaultId   int
	}

	Person struct {
		Id   int
		Name string
	}

	Log struct {
		logger log.Logger
	}
)

func (p PersonAccess) Find(id int) *Person {
	return &Person{Name: p.name, Id: id}
}

func (p PersonAccess) FindByName(name string) *Person {
	return &Person{Name: name, Id: p.id}
}

func (l *Log) Info(msg string) {
	l.logger.Println(fmt.Sprintf("Info: %v", msg))
}

func (l *Log) Error(msg string) {
	l.logger.Println(fmt.Sprintf("Error: %v", msg))
}

func createMyConfig() (*Config, error) {
	return &Config{DefaultId: 11, DefaultName: "Selene"}, nil
}

type Controller struct {
	config   *Config
	MyConfig Config           `di:"name=myconfig"`
	ignored  PersonRepository `di:"ignore"`
	repo     PersonRepository `di:"func=SetRepo"`
	Access   PersonAccess     `di:"func=SetPersonAccess,name=myconfig"`
}

func (c *Controller) SetConfig(cfg *Config) {
	c.config = cfg
}

func (c *Controller) SetRepo(r PersonRepository) {
	c.repo = r
}

func (c *Controller) SetPersonAccess(cfg Config) {
	c.Access = PersonAccess{id: cfg.DefaultId, name: cfg.DefaultName}
}

func LogMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do some useful logging
		fmt.Printf("Calling URL %v with method %v ...\n", r.URL, r.Method)
		handler.ServeHTTP(w, r)
	})
}

func FindPerson(w http.ResponseWriter, r *http.Request, repo PersonRepository, pers Person) {
	if id, err := strconv.Atoi(r.FormValue("id")); err == nil {
		w.Write([]byte(fmt.Sprintf("%v", repo.Find(id))))
	} else {
		w.Write([]byte(fmt.Sprintf("%v", pers)))
	}
}

func ChangeCfgMiddleware(c sting.Container, handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// If GetService or Inject is used inside a handler func, it's
		// necessary to pass the request context to those functions.
		cfg, _, _ := c.GetService((*Config)(nil), r.Context())
		cfg.(*Config).DefaultName = "Pumuckel"
		handler.ServeHTTP(w, r)
	})
}

// This file contains only prerequisites for all other examples. This is
// necessary because of the behavior of godoc. If an example has any
// type or function definition in it, godoc does not show a separate block
// for the output of the example.
func Example() {
	// This file contains only prerequisites for all other examples.
}
