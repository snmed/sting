package main

import (
	"bufio"
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"time"
)

const attrib = "@generr"

const headerTemplate = `// This is an auto-generated file and will be 
// overwritten next time go:generate is executed.
// Date: {{.Date}}

package {{.Name}}

`

const generrTemplate = `
// {{if eq .Comment ""}}Missing comment add one to the const declaration{{else}}{{.Comment}}{{end}}
func Is{{.Name}}(e error) bool {
	switch t := e.(type) {
	case serr:
		return t.err == {{.Value}}
	case errors:
		for _, e := range t {
			if s, ok := e.(serr); ok && s.err == {{.Value}} {
				return true
			}
		}
		return false
	default:
		return false
	}
}
`

func main() {

	var fileName = flag.String("file", "", "Specify source file that contains the error type.")
	var prefix = flag.String("prefix", "err", "Specify the prefix of the error variable names (default: err).")
	var outName = flag.String("out", "", "Specify the name of the output file.")
	flag.Parse()

	if *fileName == "" || *prefix == "" {
		flag.PrintDefaults()
		os.Exit(0)
	}

	fset := token.NewFileSet()
	fileAst, err := parser.ParseFile(fset, *fileName, nil, 0|parser.ParseComments)
	if err != nil {
		log.Fatalf("Could not parse %v", *fileName)
	}

	v := visitor{errors: &[]errorInfo{}, packageName: fileAst.Name.Name, prefix: *prefix}
	ast.Walk(v, fileAst)

	fname := *outName
	if fname == "" {
		fname = fmt.Sprintf("generated_%v", filepath.Base(*fileName))
	}

	path, err := filepath.Abs(filepath.Dir(*fileName))
	if err != nil {
		log.Fatalln(err)
	}

	outfile := filepath.Join(path, fname)
	f, err := os.OpenFile(outfile, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0777)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()
	writeCode(v, f)

}

func writeCode(v visitor, w io.Writer) {
	header := template.Must(template.New("code").Parse(headerTemplate))
	body := template.Must(template.New("code").Parse(generrTemplate))

	err := header.Execute(w, struct {
		Name string
		Date time.Time
	}{
		Name: v.packageName,
		Date: time.Now(),
	})
	if err != nil {
		log.Fatalln(err)
	}
	for _, item := range *v.errors {
		item.Name = strings.TrimPrefix(item.Value, v.prefix)
		err = body.Execute(w, item)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

type visitor struct {
	errors      *[]errorInfo
	packageName string
	prefix      string
}

type errorInfo struct {
	Name    string
	Value   string
	Comment string
}

// Visit walks through ast and extracts all sting error constants
func (v visitor) Visit(node ast.Node) ast.Visitor {
	switch t := node.(type) {
	case *ast.GenDecl:
		if t.Tok == token.CONST {
			v.processConst(t)
		}
	}
	return v
}

func (v *visitor) processConst(t *ast.GenDecl) {
	if t.Doc.Text() == "" {
		return
	}

	scanner := bufio.NewScanner(strings.NewReader(t.Doc.Text()))
	isGenErr := false
	for scanner.Scan() {
		if strings.Index(strings.TrimSpace(scanner.Text()), attrib) == 0 {
			isGenErr = true
			break
		}
	}

	if !isGenErr {
		return
	}

	for _, item := range t.Specs {
		val, ok := item.(*ast.ValueSpec)
		if ok {
			*v.errors = append(*v.errors, errorInfo{Value: val.Names[0].Name,
				Comment: strings.TrimSpace(val.Comment.Text())})
		}
	}
}
