package sting_test

import "log"
import "fmt"
import "bitbucket.org/snmed/sting"

// This example shows how to register services and dependencies.
// Register requires as first argument a struct, pointer to struct or
// a creator function and as second argument a scope.
//
// RegisterNamed requires as first argument a string under which the
// service will be registered, the other two arguments are the same
// as for the Register function.
func Example_register() {

	container, err := sting.NewBuilder().
		// Register a struct with a transient scope
		Register(Log{}, sting.TransientScope()).
		// Register a pointer to struct with a  container scope
		Register(&Config{DefaultId: 42, DefaultName: "Judge Dredd"}, sting.ContainerScope()).
		// Register a creator function with a dependency and container scope
		Register(
			func(c Config) (PersonRepository, error) {
				return PersonAccess{id: c.DefaultId, name: c.DefaultName}, nil
			},
			sting.ContainerScope(),
		).
		// Register a named service with an already registered type
		RegisterNamed(
			"myconfig",
			createMyConfig,
			sting.RequestScope(),
		).
		Build()

	if err != nil {
		log.Fatalln(err)
	}

	// Get a service by passing a nil pointer to an interface
	svc, _, err := container.GetService((*PersonRepository)(nil))
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(svc.(PersonRepository).Find(42))
	// Output: &{42 Judge Dredd}
}
